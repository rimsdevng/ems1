<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('customer.eventList');
//});

Auth::routes();

Route::get('/','PublicController@index');
Route::get('/home', 'HomeController@index')->name('home');



Route::get('create-eventCat', 'HomeController@eventCategory');
Route::post('post-EventCat', 'CustomerController@postEventCategory');

Route::get('create-event', 'CustomerController@createEvent');
Route::post('postCreateEvent', 'CustomerController@postCreateEvent');


Route::get('add-attendees/{eid}','CustomerController@addAttendees');
Route::post('post-attendees/{eid}','CustomerController@postAddAttendees');


Route::post('attend-event/{eid}','CustomerController@attendEvent');

Route::get('view-attendees/{eid}','CustomerController@viewAttendees');

//initially it was HomeController below
Route::get('download-attendees/{eid}','CustomerController@attendeesPDF');




Route::post('upload-attendees/{eid}', 'CustomerController@UploadAttendees');


Route::get('account','CustomerController@viewAccount');
Route::get('account/{uid}/edit','CustomerController@editAccount');

Route::post('account/{uid}','CustomerController@updateAccount');

Route::get('profile','CustomerController@customerProfile');
Route::post('update-user/{uid}','CustomerController@updateUser');



Route::get('edit-event/{eid}','CustomerController@editEvent');
Route::post('update-event/{eid}','CustomerController@updateEvent');
Route::get('delete-event/{eid}','CustomerController@deleteEvent');


Route::get('events','PublicController@viewEvent');
Route::get('event-details/{eid}', 'PublicController@viewEventDetails');





// Admin Routes



	Route::group(['middleware' => 'admin'], function(){



		Route::post('post-settings','HomeController@postSettings');

		Route::get('ban-user/{uid}','HomeController@banUser');

		Route::get('make-staff/{uid}','HomeController@makeStaff');
		Route::get('make-admin/{uid}','HomeController@makeAdmin');


	});


Route::group(['middleware' => 'staff'], function(){


	Route::get('admin-dash','HomeController@dashBoard');
	Route::get('user-events','HomeController@userEvents');

	Route::get('customers','HomeController@getCustomers');

	Route::get('settings','HomeController@settings');
	Route::get('settings-history','HomeController@settingsHistory');



	Route::get('ban-event/{eid}','HomeController@banEvent');
	Route::get('banned-events','HomeController@showBanned');
	Route::get('unban-event/{eid}','HomeController@unBanEvent');
	Route::get('approve-event/{eid}','HomeController@approveEvent');
	

});



Route::get('contact-us','PublicController@getContact');
Route::post('contact-us','PublicController@postContact');