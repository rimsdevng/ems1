<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Intial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table){
        	$table->increments('eid');
        	$table->integer('ecid');
			$table->integer('uid');
        	$table->string('name');
        	$table->string('location');
        	$table->string('image');
        	$table->string('state')->nullable();
        	$table->decimal('lon')->nullable();
        	$table->decimal('lat')->nullable();
        	$table->string('description',5000);
        	$table->enum('schedule',['oneTime','recurring']);
        	$table->enum('status',['draft','ongoing','ended','protected']);
        	$table->enum('eventType',['public','protected']);
        	$table->string('publishDate');
        	$table->string('endDate');
        	$table->enum('admittance',['free','paid']);
        	$table->integer('cost');
        	$table->timestamps();

        });

        Schema::create('event_categories', function (Blueprint $table){
        	$table->increments('ecid');
        	$table->string('name');
        	$table->string('description');
        	$table->integer('uid');
        	$table->timestamps();
        });

        Schema::create('attendees', function (Blueprint $table){
        	$table->increments('aid');
        	$table->integer('eid');
        	$table->string('fname');
        	$table->string('sname');
//        	$table->enum('gender',['male','female']);
        	$table->string('email');
        	$table->string('phone');
        	$table->enum('status',['paid','unpaid'])->default('unpaid');
        	$table->timestamps();
        });

        Schema::create('subscriptions', function(Blueprint $table){
            $table->increments('sid');
            $table->integer('eid');
            $table->integer('uid');
            $table->timestamps();
        });


        Schema::create('payments', function(Blueprint $table){
            $table->increments('payid');
            $table->string('amount');
            $table->string('method');
            $table->string('others');
            $table->string('uid');
            $table->timestamps();
        });

	    Schema::create('users', function (Blueprint $table) {
			$table->increments('uid');
			$table->integer('bid');
		    $table->string('fname');
		    $table->string('sname');
			$table->string('phone');
			$table->string('account_no');
			$table->string('company');
			$table->decimal('TotalEarnings');
		    $table->string('email',191)->unique();
		    $table->enum('role',['admin','user']);
		    $table->string('password');
		    $table->rememberToken();
		    $table->timestamps();
	    });


	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('password_resets');
    }
}
