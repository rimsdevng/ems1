<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        
	    Schema::create('settings', function (Blueprint $table) {
		    $table->increments('sid');
            $table->enum('name',['ChargeCap_Value','ServiceCharge_Percentage'])->default('ChargeCap_Value');
            $table->integer('value');
		    $table->timestamps();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('users');

    }
}
