@extends('layouts.admin')

@section('content')



    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->



            <section class="ls with_bottom_border">
                <div class="container-fluid">

                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3><a href="{{url('customers')}}">Customers</a></h3>
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="with_border with_padding">

                                <div class="row admin-table-filters">

                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form class="pull-right" >
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="term" class="form-control" placeholder="Search Here...">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->

                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th> #</th>
                                            <th> Name</th>
                                            <th> phone</th>
                                            <th> Company Name</th>
                                            <th> Created</th>
                                            <th> Events </th>
                                            @if(Auth::user()->role == 'admin')
                                                <th> Status </th>
                                            <th> Role </th>
                                                @endif

                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--{{$customers->links()}}--}}
                                        @foreach( $customers as $customer)

                                            <tr>
                                                <td>{{str_pad($customer->uid,3)}}</td>
                                                <td><a href="#">{{$customer->fname}} {{$customer->sname}}</a></td>

                                                <td>{{$customer->phone}}</td>

                                                <td>{{$customer->company}}</td>

                                                <td>{{$customer->created_at}}</td>

                                                @if(count($customer->Event) > 0)
                                                <td>{{count($customer->Event)}}</td>
                                                    @else
                                                    <td style="color:darkred;"> User has no Event </td>
                                                    @endif

                                                @if(Auth::user()->role == 'admin')

                                               <td>
                                                   <a href="{{url('ban-user/'.$customer->uid)}}">Ban User</a>
                                               </td>

                                                <td>
                                                    <a href="{{'make-staff/'.$customer->uid}}"><button class="btn btn-sm btn-info">Make Staff</button></a>
                                                    <a href="{{'make-admin/'.$customer->uid}}"><button class="btn btn-sm btn-danger"> Make Admin</button></a>

                                                </td>
                                                    @endif

                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="pagination">
                                        <div class=" u-MarginTop100 u-xs-MarginBottom50 u-FlexCenter">
                                            {{--<span class="u-PaddingRight50 u-PaddingLeft50 u-Weight800">{{$customers->links()}}</span>--}}
                                        </div>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>


        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->






@endsection