@extends('layouts.admin')

@section('content')



<!-- wrappers for visual page editor and boxed version of template -->
<div id="canvas">
    <div id="box_wrapper">

        <!-- template sections -->

 @include('notifications')

        <section class="ls with_bottom_border">
            <div class="container-fluid">
                {{--<div class="row">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<ol class="breadcrumb darklinks">--}}
                            {{--<li>--}}
                                {{--<a href="#">Dashboard</a>--}}
                            {{--</li>--}}
                            {{--<li class="active">Products</li>--}}
                        {{--</ol>--}}
                    {{--</div>--}}
                    {{--<!-- .col-* -->--}}
                    {{--<div class="col-md-6 text-md-right">--}}
							{{--<span class="dashboard-daterangepicker">--}}
								{{--<i class="fa fa-calendar"></i>--}}
								{{--<span></span>--}}
								{{--<i class="caret"></i>--}}
							{{--</span>--}}
                    {{--</div>--}}
                    {{--<!-- .col-* -->--}}
                {{--</div>--}}
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <h3><a href="{{url('user-events')}}">Events</a></h3>
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="with_border with_padding">
                            <h3>Event Requests From Customers</h3>
                            {{--  <div class="row admin-table-filters">  --}}
                                {{--<div class="col-lg-9">--}}

                                    {{--<form action="http://webdesign-finder.com/html/social-activism/" class="form-inline filters-form">--}}
											{{--<span>--}}
												{{--<label class="grey" for="with-selected">With Selected:</label>--}}
												{{--<select class="form-control with-selected" name="with-selected" id="with-selected">--}}
													{{--<option value="">-</option>--}}
													{{--<option value="publish">Publish</option>--}}
													{{--<option value="delete">Delete</option>--}}
												{{--</select>--}}
											{{--</span>--}}
                                        {{--<span>--}}
												{{--<label class="grey" for="orderby">Sort By:</label>--}}
												{{--<select class="form-control orderby" name="orderby" id="orderby">--}}
													{{--<option value="date" selected>Date</option>--}}
													{{--<option value="price">Price</option>--}}
													{{--<option value="title">Title</option>--}}
													{{--<option value="status">Status</option>--}}
												{{--</select>--}}
											{{--</span>--}}

                                        {{--<span>--}}
												{{--<label class="grey" for="showcount">Show:</label>--}}
												{{--<select class="form-control showcount" name="showcount" id="showcount">--}}
													{{--<option value="10" selected>10</option>--}}
													{{--<option value="20">20</option>--}}
													{{--<option value="30">30</option>--}}
													{{--<option value="50">50</option>--}}
													{{--<option value="100">100</option>--}}
												{{--</select>--}}
											{{--</span>--}}
                                    {{--</form>--}}

                                {{--</div>--}}
                                <!-- .col-* -->
                                {{--  <div class="col-lg-3 text-lg-right">
                                    <div class="widget widget_search">

                                        <form class="pull-right" >
                                            <!-- <div class="form-group-wrap"> -->
                                            <div class="form-group">
                                                <label class="sr-only" for="widget-search">Search for:</label>

                                                        <select name="by" >
                                                            <option >select filter By:</option>
                                                            <option value="name">Name</option>
                                                            <option value="location">Location</option>
                                                            <option value="company">Organizer</option>
                                                        </select>
                                                        <input id="widget-search" type="text" value="" name="term" class="form-control" placeholder="Search Here...">

                                                    </br>
                                                    <button type="submit" class="theme_button color1">Search</button>

                                            </div>

                                            <!-- </div> -->
                                        </form>
                                    </div>

                                </div>  --}}
                                <!-- .col-* -->
                            {{--  </div>  --}}
                            <!-- .row -->

                            <div class="table-responsive">
                                <table id="datatable1" class="table table-striped table-flush display">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Event Name</th>
                                        <th>Date</th>
                                         <th>Description</th>
                                        <th>Event Type</th>  
                                        <th>Expected No of Attendees</th>
                                        <th>Event Staus</th>
                                        <th>Choice of Color</th>
                                        <th>Band</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{$events->links()}}
                                    @foreach( $events as $event)
                                        <tr>
                                            {{--<td>{{  }}str_pad($event-eid,3)}}</td>--}}
                                            <td>{{$event->eid}}</td>
                                          <td> <a href="{{url('event-details/'.$event->eid)}}">{{$event->name}}</a></td>
                                          <td>{{$event->created_at}}</td>
                                            <td>{{$event->description}}</td>
                                            <td>{{$event->type}}</td>  
                                            <td>{{ $event->number_of_attendees }}</td>
                                            <td>{{ $event->status }}</td>
                                            <td>{{ $event->color }}</td>

                                            {{--  @if($event->admittance == 'free')
                                                <td> Free Event</td>
                                            @else
                                            <td>{{$event->cost}}</td>
                                                @endif  --}}

                                            @if(Auth::check() and Auth::user()->role =='admin' or Auth::user()->role == 'staff' )


                                                {{--<form method="post" action="{{url('ban-event/'.$event->eid)}}">--}}
                                                    {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}

                                                    {{--<td><a class="btn btn-danger" type="submit"> Ban Event </a></td>--}}
                                                {{--</form>--}}


                                            <td><a href="{{url('ban-event/'.$event->eid)}}"> Ban Event </a></td>
                                            <td><a href="{{url('approve-event/'.$event->eid)}}"> Approve Event </a></td>
                                            
                                                @endif
                                        </tr>
                                        
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- .table-responsive -->
                        </div>
                        <!-- .with_border -->
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="pagination">
                                    <div class=" u-MarginTop100 u-xs-MarginBottom50 u-FlexCenter">
                                        <span class="u-PaddingRight50 u-PaddingLeft50 u-Weight800">{{$events->links()}}</span>
                                    </div>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- .row main columns -->
            </div>
            <!-- .container -->
        </section>


    </div>
    <!-- eof #box_wrapper -->
</div>
<!-- eof #canvas -->






@endsection