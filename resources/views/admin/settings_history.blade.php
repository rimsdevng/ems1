@extends('layouts.admin')

@section('content')


    <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <h3>Settings History</h3>
                </div>
            </div>





            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Settings</th>
                    <th>Value</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>


                <tr>
                    @foreach($settings as $s)<td>{{$s->name}} </td>

                    <td>{{ $s->value }}</td>
                    <td>{{$s->created_at}}</td>

                </tr>
                @endforeach

                </tbody>
            </table>



            <div class="row flex-row">

                <div class="col-md-6">
                    <div class="with_border with_padding">

                        <ul class="list1 no-bullets">



                        </ul>

                    </div>
                    <!-- .muted_background -->
                </div>
                <!-- .col-* -->
                <div class="col-md-6">
                    <div class="with_border with_padding">



                    </div>
                </div>
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </section>

@endsection