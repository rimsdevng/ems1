@extends('layouts.admin')

@section('content')

<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-4">
							<h3 class="dashboard-page-title">Dashboard
								<small>main page</small>
							</h3>
						</div>
						{{--  <div class="col-md-8 text-md-right">
							<h3 class="sparklines-title">
								<sup>Today Earnings:</sup>

								3,000

								<span class="sparklines" data-values="670,350,135,-170,-324,-386,-468,-200,55,375,520,270,790,-670,-350,135,170,324,386,468,10,55,375,520,270,790" data-type="bar" data-line-color="#eeb269" data-neg-color="#dc5753" data-height="30" data-bar-width="2">
								</span>

							</h3>

							<h3 class="sparklines-title">
								<sup>Yesterday Earn: </sup>
								4,000

								<span class="sparklines" data-values="670,350,135,-170,-324,386,-468,-10,55,375,520,-270,790,670,-350,135,170,324,386,468,10,-55,-375,-520,270,790" data-type="bar" data-line-color="#4db19e" data-neg-color="#007ebd" data-height="30" data-bar-width="2">
								</span>
							</h3>  --}}

						</div>

					</div>
					<!-- .row -->


					<div class="row">
						<div class="col-lg-3 col-sm-6">

							<div class="teaser warning_bg_color counter-background-teaser text-center">
								<span class="counter counter-background" data-from="0" data-to="1257" data-speed="2100">0</span>
								<h3 class="counter highlight" data-from="0" data-to="{{count($events)}}" data-speed="2100">0</h3>
								<p>Customers/Wk</p>
							</div>

						</div>

						<div class="col-lg-3 col-sm-6">

							<div class="teaser danger_bg_color counter-background-teaser text-center">
								<span class="counter counter-background" data-from="0" data-to="346" data-speed="1500">0</span>
								<h3 class="counter highlight" data-from="0" data-to="{{count($events)}}" data-speed="1500">0</h3>
								<p><b>Events/Wk</b></p>
							</div>

						</div>

						<div class="col-lg-3 col-sm-6">

							<div class="teaser success_bg_color counter-background-teaser text-center">
								<span class="counter counter-background" data-from="0" data-to="216" data-speed="1900">0</span>
								<h3 class="counter highlight" data-from="0" data-to="{{ count($events) }}" data-speed="1900">0</h3>
								<p>Orders / Month</p>
							</div>

						</div>

						<div class="col-lg-3 col-sm-6">

							<div class="teaser info_bg_color counter-background-teaser text-center">
								<span class="counter counter-background" data-from="0" data-to="15" data-speed="1800">0</span>
								<h3 class="counter-wrap highlight" data-from="0" data-to="346" data-speed="1800">
									<small></small>
									<span class="counter" data-from="0" data-to="{{ count($events) }}" data-speed="1500">0</span>
									<small class="counter-add">k</small>
								</h3>
								<p>Total Profit</p>
							</div>

						</div>
					</div>




					<!-- .row -->


					<div class="row">


                        <div class="col-xs-12 col-lg-6">
                            <div class="with_border with_padding">
                                <h4>
                                    Recent Events
                                </h4>
                                <hr>
                                <div class="admin-scroll-panel scrollbar-macosx">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#Id</th>
                                            <th>Event Name</th>
                                            <th>Created</th>
                                            <th>Organizers</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($events as $event)
                                        <tr>

                                            <td>{{$event->eid}}</td>
                                            <td>{{$event->name}}</td>
                                            <td>{{$event->created_at->diffForHumans()}}</td>
                                            <td>{{$event->company}}</td>

                                        </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    <a class="btn btn-sm pull-right" href="{{url('user-events')}}"> View More</a>

                                </div>
                                <!-- .admin-scroll-panel -->
                            </div>
                            <!-- .with_border -->
                        </div>
						<!-- .col-* -->

						<div class="col-xs-12 col-lg-6">
							<div class="with_border with_padding">
								<h4>
									Recent Customers
								</h4>
								<hr>
								<div class="admin-scroll-panel scrollbar-macosx">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @foreach($users as $user)
                                            <td>{{$user->uid}}</td>
                                            <td>{{$user->fname}}</td>
                                            <td>{{$user->sname}}</td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    <a class="btn btn-sm pull-right" href="{{url('customers')}}"> View More</a>
								</div>
								<!-- .admin-scroll-panel -->
							</div>
							<!-- .with_border -->
						</div>
					</div>


					<!-- .row -->




				</div>
				<!-- .container -->
			</section>

@endsection