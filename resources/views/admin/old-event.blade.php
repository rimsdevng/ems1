






//bootstrap tables






<!-- search modal -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">
				<i class="rt-icon2-cross2"></i>
			</span>
    </button>
    <div class="widget widget_search">
        <form method="get" class="searchform search-form form-inline" action="http://webdesign-finder.com/html/social-activism/">
            <div class="form-group">
                <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
            </div>
            <button type="submit" class="theme_button">Search</button>
        </form>
    </div>
</div>

<!-- Unyson messages modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
    <div class="fw-messages-wrap ls with_padding">
        <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
        <!--
    <ul class="list-unstyled">
        <li>Message To User</li>
    </ul>
    -->

    </div>
</div>
<!-- eof .modal -->



<!-- wrappers for visual page editor and boxed version of template -->




<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_5">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-12">
                <h3>Tables
                    <small>included</small>
                </h3>
            </div>
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="with_border with_padding">




                    <h3 class="divider_40">Bootstrap Stripped Table</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $events as $event)
                            <tr>
                                <td>{{$event->name}}</td>
                                <td>{{$event->created_at}}</td>
                                <td>{{$event->company}}</td>
                                <td>@mdo</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>


                    {{--<h3 class="divider_40">Template table</h3>--}}
                    {{--<table class="table_template">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                    {{--<th>#</th>--}}
                    {{--<th>First Name</th>--}}
                    {{--<th>Last Name</th>--}}
                    {{--<th>Username</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--<tr>--}}
                    {{--<td rowspan="2">1</td>--}}
                    {{--<td>Mark</td>--}}
                    {{--<td>Otto</td>--}}
                    {{--<td>@mdo</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td>Mark</td>--}}
                    {{--<td>Otto</td>--}}
                    {{--<td>@TwBootstrap</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td>2</td>--}}
                    {{--<td>Jacob</td>--}}
                    {{--<td>Thornton</td>--}}
                    {{--<td>@fat</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                    {{--<td>3</td>--}}
                    {{--<td colspan="2">Larry the Bird</td>--}}
                    {{--<td>@twitter</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                    {{--</table>--}}

                </div>
                <!-- .with_border -->

            </div>
            <!-- .col-* -->
        </div>
        <!-- .row -->


        <div class="row topmargin_40">
            <div class="col-sm-12">


                <h3 class="divider_40">Bootstrap Stripped Table</h3>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>
                </table>



            </div>
            <!-- .col-* -->
        </div>
        <!-- .row -->

    </div>
    <!-- .container -->
</section>



</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas -->


<!-- chat -->
<div class="side-dropdown side-chat dropdown">
    <a class="side-button side-chat-button" id="chat-dropdown" data-target="#" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-comments-o"></i>
    </a>

    <ul class="dropdown-menu list-unstyled" aria-labelledby="chat-dropdown">
        <li class="dropdown-title darkgrey_bg_color">
            <h4>
                Small Chat
                <span class="pull-right">14.03.2017</span>
            </h4>
        </li>
        <li>

            <ul class="list-unstyled">
                <li class="side-chat-item item-secondary">
                    <h5>
                        Michael Anderson
                        <time class="pull-right small-chat-date" datetime="2017-03-13T08:50:40+00:00">
                            08:50
                        </time>
                    </h5>
                    <div class="danger_bg_color">
                        Duis autem veum iriure dolor in hendrerit
                    </div>
                </li>
                <li class="side-chat-item item-primary">
                    <h5>
                        Jane Walker
                        <time class="pull-right small-chat-date" datetime="2017-03-13T08:50:40+00:00">
                            08:52
                        </time>
                    </h5>
                    <div class="warning_bg_color">
                        Vulputate vese molestie consequatl illum
                    </div>
                </li>
                <li class="side-chat-item item-secondary">
                    <h5>
                        Michael Anderson
                        <time class="pull-right small-chat-date" datetime="2017-03-13T08:50:40+00:00">
                            08:50
                        </time>
                    </h5>
                    <div class="danger_bg_color">
                        Duis autem veum iriure dolor in hendrerit
                    </div>
                </li>
            </ul>
        </li>


        <li role="separator" class="divider"></li>

        <li>
            <div class="side-chat-answer">
                <form class="form-inline button-on-input">
                    <div class="form-group">
                        <label for="side-chat-message" class="sr-only">Message</label>
                        <input type="text" class="form-control" id="side-chat-message" placeholder="Message">
                    </div>
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-paper-plane-o"></i>
                    </button>
                </form>
            </div>
        </li>
    </ul>
</div>
<!-- .chat-dropdown -->


<a class="side-button side-contact-button" data-target="#admin_contact_modal" href="#admin_contact_modal" data-toggle="modal" role="button">
    <i class="fa fa-envelope"></i>
</a>





