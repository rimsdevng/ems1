@extends('layouts.app')

@section('content')
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">My profile</div>--}}

                    {{--<div class="panel-body">--}}
                        {{--<div  class="row col-md-12" >--}}
                            {{--<span>--}}
                                {{--<div class="col-md-3">--}}
                               {{--<button class="btn btn-primary ">My events</button>--}}
                                {{--</div>--}}
                            {{--</span>--}}
                        {{--<span>--}}
                               {{--<div class="col-md-3">--}}
                                   {{--<button class="btn btn-primary ">Account Information</button>--}}
                               {{--</div>--}}
                        {{--</span>--}}
                            {{--<span>--}}
                                {{--<div class="col-md-3">--}}
                                   {{--<button class="btn btn-primary "> Earnings</button>--}}
                               {{--</div>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}

            {{--@foreach($events as $event)--}}

                {{--@if(count($event) > 0  )--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}
                            {{--<a href=" {{url('event-details/'. $event->eid)}}"><h4>  {{$event->name}}</h4></a>--}}
                            {{--<span>  {{$event->category->name}}</span>--}}
                            {{--<span>  {{$event->created_at}}</span>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<span>--}}
                                {{--<ul>--}}
                                   {{--<a href="{{url('edit-event/'. $event->eid)}}"> <li> edit </li></a>--}}
                                    {{--<a href="{{url('delete-event/'. $event->eid)}}"> <li> Delete </li></a>--}}
                                    {{--<a href="{{url('add-attendees/'. $event->eid)}}"> <li> Add attendes </li></a>--}}
                                {{--</ul>--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@else--}}
                {{--<h2> Hey you don't have any event yet, <a href="{{url('create-event')}}">Make some</a></h2>--}}
                     {{--<button class="btn btn-primary"> make some now!</button>--}}
            {{--@endif--}}
                {{--@endforeach--}}
        {{--</div>--}}
    {{--</div>--}}




    <section class="ls section_padding_top_100 section_padding_bottom_100 columns_padding_25">
        <div class="container">
            <div class="row">
                <div class="col-md-5">

                    <div class="vertical-item content-padding with_border text-center  ">
                        <div class="item-media"><br>
                            {{-- <img src="assets/images/logo.png" alt="" /> --}}
                            <h3>Event Sparks</h3>
                        </div>
                        <div class="item-content">
                            <h3 class="bottommargin_0 ">
                                <a href="{{ url('/') }}">{{Auth::user()->fname}}, {{Auth::user()->sname}}</a>
                            </h3>
                            @var
                            <p class="small-text highlight">+233238855067</p><br>
                            <p class="small-text highlight">{{Auth::user()->account_no}}</p><br>

                            <div>
                                 {{--  <p>You have made</p>
                                
                                <b> GH&#8373 {{number_format($earnings,2) }}</b>
                                <p> with us, congratulation.</p>  --}}
                            </div>


                            <a href="{{url('create-event')}}" class="btn ">Create New Event</a>
                            <p class="greylinks">
                                <a href="#" class="social-icon soc-facebook"> </a>
                                <a href="#" class="social-icon soc-twitter"></a>
                                <a href="#" class="social-icon soc-google"></a>
                            </p>
                        </div>
                    </div>

                </div>

                <div class="col-md-7">



                    <ul class="list2 checklist darklinks semibold">
                        <li>
                            <a href="#"><b>EMAIL:</b> {{Auth::user()->email}}</a>
                        </li>

                        <li>
                            <a href="#"><b>PHONE:</b> {{Auth::user()->phone}}</a>
                        </li>

                        {{-- <li>
                            <a href="#"><b>Bank:</b> {{Auth::user()->Bank->name}}</a>
                        </li> --}}


                        <li>

                            <a href="#"><b>Account Numbet:</b> {{Auth::user()->account_no}}</a>
                        </li>
                    </ul>


                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs topmargin_40" role="tablist">
                        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">My Events</a></li>
                        {{--  <li><a href="#tab2" role="tab" data-toggle="tab">Payment Info</a></li>  --}}
                        <li><a href="#tab3" role="tab" data-toggle="tab">Account Settings</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content big-padding top-color-border bottommargin_40">

                        <div class="tab-pane fade in active" id="tab1">

                            @if(count($events) > 0)
                            @foreach($events as $event)

                            <article class="post side-item content-padding with_border ">

                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="item-media">
                                            <img src="{{$event->image}}" alt="">
                                            <div class="media-links">
                                                <a class="abs-link" title="" href="singleevent.html"></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <div class="item-content">
                                            <h4>
                                                <a href="{{url('event-details/'.$event->eid)}}">{{$event->name}}</a>
                                            </h4>

                                            <p class=" grey darklinks">
                                                <span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> {{$event->created_at}}</span>
                                                <i class="fa fa-map-marker rightpadding_5 highlight"></i> {{$event->state}}
                                            </p>
                                            <p>{{str_limit($event->description,70)}} </p>

                                        </div>

                                        <div class="row">
                                            <a href="{{url('event-details/'.$event->eid)}}">View</a>
                                            <a href="{{url('edit-event/'.$event->eid)}}">Edit</a>
                                            <a href="{{url('delete-event/'.$event->eid)}}">Delete</a>

                                            <a class="pull-right" href="{{url('add-attendees/'.$event->eid)}}">Add Attendees</a>
                                            <a class="pull-right" href="{{url('view-attendees/'.$event->eid)}}">view Attendees</a>

                                        </div>
                                    </div>

                                </div>
                            </article>

                                @endforeach

                            @else
                                <h3>You do not have any events yet! <a class="text-center"  href="{{url('create-event')}}"> make here!</a></h3>
                            @endif

                            {{$events->links()}}



                        </div>
{{--  TAB TWO WAS HERE  --}}

                        <div class="tab-pane fade" id="tab3">
                            <form class="contact-form" method="post" action="">
                                <p class="contact-form-name">
                                    <!-- <label for="name">Name <span class="required">*</span></label> -->
                                    <input type="text" aria-required="true" size="30" value="" name="name" class="form-control" placeholder="Name">
                                </p>
                                <p class="contact-form-email">
                                    <!-- <label for="email">Email <span class="required">*</span></label> -->
                                    <input type="email" aria-required="true" size="30" value="" name="email" class="form-control" placeholder="Email Address">
                                </p>
                                <p class="contact-form-email">
                                    <!-- <label for="email">Email <span class="required">*</span></label> -->
                                    <input type="number" aria-required="true" size="30" value="" name="number" class="form-control" placeholder="Phone Number">
                                </p>
                                <p class="contact-form-submit topmargin_30">
                                    <button type="submit" name="contact_submit" class="theme_button color1">Update</button>
                                </p>
                            </form>
                        </div>
                    </div>
                    <!-- eof .tab-content -->



                </div>

            </div>
        </div>
    </section>




@endsection
