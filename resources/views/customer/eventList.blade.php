@extends('layouts.app')

@section('content')
                        {{--@foreach($events as $event)--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 mix april-14 april-22 hvr-float-shadow wow fadeIn">--}}
                            {{--<div class="img-holder"><img src="{{$event->image}}" alt=""></div>--}}
                            {{--<div class="content-wrap">--}}
                                {{--<img src="{{$event->image}}" alt="" class="author-img">--}}
                                {{--<div class="meta">--}}
                                    {{--<ul>--}}
                                        {{--<li><span><i class="fa fa-clock-o"></i>{{$event->publishDate}}</span></li>--}}
                                        {{--<li><span><i class="fa fa-map-marker"></i>{{$event->state}}</span></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                                {{--<h3>{{$event->name}} </h3>--}}
                                {{--<p>{{str_limit($event->description,50)}}</p>--}}
                                {{--<a class="read-more" href="{{url('event-details/'.$event->eid)}}">View Event<i class="fa fa-angle-right"></i></a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--@endforeach--}}




                        <!-- search modal -->
                        <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">
				<i class="rt-icon2-cross2"></i>
			</span>
                            </button>
                            <div class="widget widget_search">
                                <form method="get" class="searchform search-form form-inline" action="">
                                    <div class="form-group">
                                        <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                                    </div>
                                    <button type="submit" class="theme_button">Search</button>
                                </form>
                            </div>
                        </div>

                        <!-- Unyson messages modal -->
                        <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
                            <div class="fw-messages-wrap ls with_padding">
                                <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
                                <!--
                            <ul class="list-unstyled">
                                <li>Message To User</li>
                            </ul>
                            -->

                            </div>
                        </div>
                        <!-- eof .modal -->

                        <!-- wrappers for visual page editor and boxed version of template -->
                        <div id="canvas">
                            <div id="box_wrapper">

                                <!-- template sections -->






                                <section class="ls section_padding_top_50 section_padding_bottom_130 columns_padding_25">
                                    <div class="container">
                                        <div class="row">

                                            <div class="col-sm-7 col-md-8 col-lg-8 col-sm-push-5 col-md-push-4 col-lg-push-4">

                                                    @foreach($events as $event)
                                                <article class="post side-item content-padding with_border ">

                                                    <div class="row">

                                                        {{--  <div class="col-md-5">
                                                            <div class="item-media">
                                                                <img src="assets/images/events/02.jpg" alt="">
                                                                <div class="media-links">
                                                                    <a class="abs-link" title="" href="singleevent.html"></a>
                                                                </div>
                                                            </div>
                                                        </div>  --}}

                                                        @if($event->status == "ongoing")
                                                        
                                                        <div class="col-md-7">
                                                            <div class="item-content">
                                                                <h4>
                                                                    <a href="{{url('event-details/'.$event->eid)}}">{{$event->name}}</a>
                                                                </h4>

                                                                <h6 style="color: silver">
                                                                    {{$event->category->name}}
                                                                </h6>

                                                                <p class=" grey darklinks">
                                                                    <span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> {{$event->publishDate}}</span>
                                                                    <i class="fa fa-map-marker rightpadding_5 highlight"></i> {{$event->state}}
                                                                </p>
                                                                <p>{{str_limit($event->description,100)}}</p><br>
                                                                <a class="btn rounded " href="{{url('event-details/'.$event->eid)}}"> View Event</a>

                                                            </div>
                                                        </div>
                                                        @else
                                                            <p class="grey darklinks">No new Event Published</p>
                                                        
                                                        
                                                        @endif


                                                    </div>
                                                </article>
                                                @endforeach

                                                {{--<article class="post side-item content-padding with_border ">--}}

                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-5">--}}
                                                            {{--<div class="item-media">--}}
                                                                {{--<img src="assets/images/events/04.jpg" alt="">--}}
                                                                {{--<div class="media-links">--}}
                                                                    {{--<a class="abs-link" title="" href="singleevent.html"></a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-7">--}}
                                                            {{--<div class="item-content">--}}
                                                                {{--<h4>--}}
                                                                    {{--<a href="singleevent.html">Silverbird makeup Gala</a>--}}
                                                                {{--</h4>--}}

                                                                {{--<p class=" grey darklinks">--}}
                                                                    {{--<span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> Feb 12, 2017</span>--}}
                                                                    {{--<i class="fa fa-map-marker rightpadding_5 highlight"></i> Conference Hall--}}
                                                                {{--</p>--}}
                                                                {{--<p>Christmas comes early as Pops Concepts organisers of Nigeria’s largest MakeUp & Beauty Trade Show “The MakeUp Fair Series”.</p>--}}


                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</article>--}}


                                                {{--<article class="post side-item content-padding with_border ">--}}

                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-5">--}}
                                                            {{--<div class="item-media">--}}
                                                                {{--<img src="assets/images/events/01.jpg" alt="">--}}
                                                                {{--<div class="media-links">--}}
                                                                    {{--<a class="abs-link" title="" href="singleevent.html"></a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-7">--}}
                                                            {{--<div class="item-content">--}}
                                                                {{--<h4>--}}
                                                                    {{--<a href="singleevent.html">Ikoyi makeup Fair</a>--}}
                                                                {{--</h4>--}}

                                                                {{--<p class=" grey darklinks">--}}
                                                                    {{--<span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> Feb 12, 2017</span>--}}
                                                                    {{--<i class="fa fa-map-marker rightpadding_5 highlight"></i> Conference Hall--}}
                                                                {{--</p>--}}
                                                                {{--<p>Christmas comes early as Pops Concepts organisers of Nigeria’s largest MakeUp & Beauty Trade Show “The MakeUp Fair Series”.</p>--}}


                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</article>--}}


                                                {{--<article class="post side-item content-padding with_border ">--}}

                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-5">--}}
                                                            {{--<div class="item-media">--}}
                                                                {{--<img src="assets/images/events/02.jpg" alt="">--}}
                                                                {{--<div class="media-links">--}}
                                                                    {{--<a class="abs-link" title="" href="singleevent.html"></a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-7">--}}
                                                            {{--<div class="item-content">--}}
                                                                {{--<h4>--}}
                                                                    {{--<a href="singleevent.html">Maitama Makeup fair</a>--}}
                                                                {{--</h4>--}}

                                                                {{--<p class=" grey darklinks">--}}
                                                                    {{--<span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> Feb 12, 2017</span>--}}
                                                                    {{--<i class="fa fa-map-marker rightpadding_5 highlight"></i> Conference Hall--}}
                                                                {{--</p>--}}
                                                                {{--<p>Christmas comes early as Pops Concepts organisers of Nigeria’s largest MakeUp & Beauty Trade Show “The MakeUp Fair Series”.</p>--}}

                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</article>--}}

                                                {{--<article class="post side-item content-padding with_border ">--}}

                                                    {{--<div class="row">--}}

                                                        {{--<div class="col-md-5">--}}
                                                            {{--<div class="item-media">--}}
                                                                {{--<img src="assets/images/events/04.jpg" alt="">--}}
                                                                {{--<div class="media-links">--}}
                                                                    {{--<a class="abs-link" title="" href="singleevent.html"></a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-7">--}}
                                                            {{--<div class="item-content">--}}
                                                                {{--<h4>--}}
                                                                    {{--<a href="singleevent.html">Wuse makeup fair</a>--}}
                                                                {{--</h4>--}}

                                                                {{--<p class=" grey darklinks">--}}
                                                                    {{--<span class="rightpadding_20"><i class="fa fa-calendar rightpadding_5 highlight"></i> Feb 12, 2017</span>--}}
                                                                    {{--<i class="fa fa-map-marker rightpadding_5 highlight"></i> Conference Hall--}}
                                                                {{--</p>--}}
                                                                {{--<p>Christmas comes early as Pops Concepts organisers of Nigeria’s largest MakeUp & Beauty Trade Show “The MakeUp Fair Series”.</p>--}}

                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                    {{--</div>--}}
                                                {{--</article>--}}


                                                <div class="row topmargin_60">
                                                    <div class="col-sm-12 text-center">
                                                        <ul class="pagination">
                                                            <li class="disabled">
                                                                <a href="#">
                                                                    <span class="sr-only">Prev</span>
                                                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                            <li class="active"><a href="#">1</a></li>
                                                            <li><a href="#">2</a></li>
                                                            <li><a href="#">3</a></li>
                                                            <li><a href="#">4</a></li>
                                                            <li>
                                                                <a href="#">
                                                                    <span class="sr-only">Next</span>
                                                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--eof .col-sm-8 (main content)-->

                                            <!-- sidebar -->
                                            <aside class="col-sm-5 col-md-4 col-lg-4 col-sm-pull-7 col-md-pull-8 col-lg-pull-8">






                                                <div class="widget widget_categories">

                                                    <h3 class="widget-title">Search</h3>
                                                    <div class="form-group select-group">
                                                        <label for="category" class="sr-only">search</label>
                                                        <form>
                                                            <input class="form-control" placeholder="please find event by name" name="search">
                                                            <i class="fa fa-angle-down theme_button no-bg-button" aria-hidden="true"></i>
                                                            <button class="btn btn-info  pull-left" type="submit" >Go</button>

                                                        </form>

                                                    </div>
                                                </div>



                                                <div class="widget widget_archive">

                                                    <h3 class="widget-title">Categories</h3>
                                                    <div class="form-group select-group">
                                                        <label for="archive" class="sr-only">Select Category</label>

                                                        {{--method="post" action="{{url('events')}}"--}}
                                                        <form >
                                                            {{--{{csrf_field()}}--}}
                                                            <select id="archive" name="cat" class="choice empty form-control">
                                                                @foreach($categories as $cat)
                                                                    <option value="{{$cat->ecid}}">{{$cat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <i class="fa fa-angle-down theme_button no-bg-button" aria-hidden="true"></i>

                                                            <button class="btn btn-info  pull-left" type="submit" >Go</button>

                                                        </form>



                                                    </div>
                                                </div>
                                                {{--<div class="widget widget_archive">--}}

                                                    {{--<h3 class="widget-title"></h3>--}}
                                                    {{--<div class="form-group select-group">--}}
                                                        {{--<label for="archive" class="sr-only">Select Month</label>--}}
                                                        {{--<select id="archive" name="archive" class="choice empty form-control">--}}
                                                            {{--<option value="" disabled="" selected="" data-default="">Select Date</option>--}}
                                                            {{--<option value="paid">Paid Events</option>--}}
                                                            {{--<option value="free">Free Events</option>--}}
                                                            {{----}}
                                                        {{--</select>--}}
                                                        {{--<i class="fa fa-angle-down theme_button no-bg-button" aria-hidden="true"></i>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                <div class="widget widget_archive">

                                                    <h3 class="widget-title">Price</h3>
                                                    <div class="form-group select-group">
                                                        <label for="archive" class="sr-only">Select Month</label>

                                                        <form>
                                                            <select id="archive" name="price" class="choice empty form-control">
                                                                <option value="" disabled="" selected="" data-default="">Price</option>
                                                                <option value="free"> Free Events</option>
                                                                <option value="paid">Paid Events</option>

                                                            </select>
                                                            <i class="fa fa-angle-down theme_button no-bg-button" aria-hidden="true"></i>
                                                            <button class="btn btn-info  pull-left" type="submit" >Go</button>
                                                        </form>

                                                    </div>
                                                </div>

                                            </aside>
                                            <!-- eof aside sidebar -->

                                        </div>
                                    </div>
                                </section>





    <script>


        $(document).ready(function(){
            var term = $("#search");

                $(term).on('keyup',function() {
                    console.log(term.val());

                $.ajax({
                    url:"api/events-search",
                    data:{

                        term : term.val()

                    },
                    success: function (res) {

                         console.log(res);

                    }
                });
            })



        });


    </script>


@endsection


