rough
===========

{{-- upcoming event --}}

<section id="upcoming-event">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-7">
                <div class="section-title">
                    <h1>Popular Events</h1>
                </div>
                <p>This weeks list of trending events across all categories</p>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5">
                <form action="#" class="pull-right">
                    <input type="text" placeholder="Search Event"> <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-title-wrap">
                    <ul class="clearfix">
                        <li class="filter active" data-filter="all"><span>All Event</span></li>
                        <li class="filter" data-filter=".april-14"><span>Dec 14</span></li>
                        <li class="filter" data-filter=".april-22"><span>Dec 22</span></li>
                        <li class="filter" data-filter=".april-28"><span>Dec 28</span></li>
                        <li class="filter" data-filter=".may-10"><span>Jan 10</span></li>
                        <li class="filter" data-filter=".may-15"><span>Jan 15</span></li>
                        <li class="filter" data-filter=".may-22"><span>Jan 22</span></li>
                        <li class="filter" data-filter=".may-23"><span>Feb 23</span></li>
                        <li class="filter" data-filter=".jun-01"><span>March 01</span></li>
                    </ul>
                </div>
                <div class="tab-content-wrap row">

                    @foreach($events as $event)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix april-14 april-22 hvr-float-shadow wow fadeIn">
                        <div class="img-holder"><img src="img/upcoming-event/5.jpg" alt=""></div>
                        <div class="content-wrap">
                            <img src="img/upcoming-event/author.png" alt="" class="author-img">
                            <div class="meta">
                                <ul>
                                    <li><span><i class="fa fa-clock-o"></i>{{$event->created_at->diffForHumans()}}</span></li>
                                    <li><span><i class="fa fa-map-marker"></i>{{$event->state}}</span></li>
                                </ul>
                            </div>
                            <h3>{{$event->name}}</h3>
                            <p>{{str_limit($event->description,100)}}</p>
                            <a class="read-more" href="{{url('event-details/'.$event->eid)}}">view Event<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>

                   @endforeach

                </div>
            </div>
        </div>
    </div>
</section>



{{-- blog --}}

<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-center">
                    <h1>CATEGORIES</h1>
                </div>
            </div>
        </div>
        <div class="row blog-post-container">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="single-post thumbnail col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                        <div class="img-holder pull-left">
                            <img src="img/blog/8.jpg" alt="">
                        </div>
                        <div class="content pull-left">
                            <h3>Hackathon Conference</h3>
                            <p class="meta"><a href="#">Bola Bewe</a> on 4  Jan, 2018</p>
                            <p>Come crack your ideas with with the   <br> for those in the Information industry.</p>
                            <a href="events.html" class="read-more hvr-bounce-to-right">View Networking</a>
                        </div>
                    </div>
                    <div class="single-post thumbnail col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                        <div class="img-holder pull-left">
                            <img src="img/blog/1.jpg" alt="">
                        </div>
                        <div class="content pull-left">
                            <h3>Music/Concert Events</h3>
                            <p class="meta"><a href="#">Bola Bewe</a> on 4  Jan, 2018</p>
                            <p>WonderKid special live performance  <br> for those in the music industry.</p>
                            <a href="events.html" class="read-more hvr-bounce-to-right">View Music</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-lg-push-0 col-md-push-0 col-sm-push-3">
                <div class="single-post wow fadeIn">
                    <div class="img-holder">
                        <img src="img/blog/7.jpg" alt="">
                    </div>
                    <div class="content">
                        <h3>Special Events</h3>
                        <p class="meta"><a href="#">David Lekan</a> on 11  Feb, 2018</p>
                        <p>Capitalism Ambassador Progra <br> International Conference Center.</p>
                        <a href="events.html" class="read-more hvr-bounce-to-right">View More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




