<?php use Illuminate\Support\Facades\Input; use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')




<!-- #blog -->
<section id="blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="section-title text-center">
                    <h1>Attendees</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10">
                    <button class="btn btn-success mt-3" onclick="window.print();"><i class="fa fa-print"></i> Print</button>  


                 <div class="section-title ">
                        <form>
                    <select class="" name="by">
                        <option value=" ">All</option>
                        <option value="paid"
                        @if(Input::get('by') == 'paid')
                            selected
                                @endif
                        >Paid</option>
                        <option value="unpaid"
                        @if(Input::get('by')== 'unpaid')
                            selected
                                @endif
                        >Unpaid</option>
                    </select>
                            {{--  <a href="{{url('download-attendees/{eid}')}}" class="btn btn-sm btn-primary"> Sort </a>  --}}
                        </form>

                </div>  
            </div>

            {{--  <div class="col-lg-2 pull-left">
                <button class="btn btn-info"> Download Attendees From the Formats below</button>

            </div>  --}}
        </div>



        <div class="row">
          <span style="color: darkred"> Event Name: <h5>{{$event->name}}</h5></span>

        </div>

        


            <div class="row">
            <table id="datatable1" class="table col-md-10 table table-hover display">
                <tr>
                    <th>First Name</th>
                    <th>last Name</th>
                    <th>Email</th>
                    <th class="quantityTH">Status</th>
                    <th></th>
                </tr>
                @if(count($attendees)> 0)
                @foreach($attendees as $a)


                <tr>
                    <td>
                        {{$a->fname}}
                    </td>
                    <td>
                        {{$a->sname}}
                    </td>
                    <td>
                        {{$a->email}}
                    </td>
                      @if($a->status == 'unpaid')
                    <td style="color: darkred;">  {{$a->status}} </td>
                          @endif

                    @if($a->status == 'paid')
                    <td style="color: darkgreen;">  {{$a->status}} </td>
                          @endif
                </tr>



                @endforeach

                    @endif



            </table>

                @if(count($attendees) == 0)

                    <h3 class="text-center"> You have no Attendees yet!</h3>

                @endif

            </div>



<a class="btn btn-primary" href="{{url('profile')}}"> Back to Profile</a>


    </div>
</section>
<!-- /#blog -->





@endsection






