@extends('layouts.app')


@section('content')


    <section id="contact" class="gradient-overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-section">
                    <form class="contact-form" action="{{url('contact-us')}}">
                        <ul class="clearfix">
                            <li class="half"><input style="color: white" type="text" name="name" placeholder="Your Name"></li>
                            <li class="half"><input style="color: white" type="email" name="email" placeholder="Your Email"></li>
                            <li class="full"><input style="color: white" type="text" name="subject" placeholder="Your Subject"></li>
                            <li class="full"><textarea style="color: white" name="message" placeholder="Your Message here"></textarea></li>
                            <li class="full"><button  type="submit" class="hvr-bounce-to-right">Contact Us</button></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </section>



@endsection