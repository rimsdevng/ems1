@extends('layouts.app')

@section('content')

    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}

                {{--<div class=" container ">--}}
                    {{--<form class=" pull-right form-inline" style="padding-top: 14px width:80%;">--}}

                        {{--<input name="term" id="search" type="text" class="form-control" placeholder="Search for a Post">--}}
                        {{--<button class="btn btn-primary">GO</button>--}}
                        {{--<br>--}}
                    {{--</form>--}}
                {{--</div>--}}
               {{----}}
                {{--@foreach($events as $event)--}}
                    {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-body">--}}
                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                             {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}
                            {{--<a href=" {{url('event-details/'. $event->eid)}}">  <h3>  {{$event->name}}</h3></a>--}}
                              {{--<span>  {{$event->category->name}}</span>--}}
                              {{--<span>  {{$event->created_at}}</span>--}}
                            {{--<div class="col-md-6">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>@endforeach--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}








    <!-- /#header -->

    <!-- #banner -->
    <section id="banner">
        <div class="banner-container">
            <div class="banner">
                <ul>

                    <li
                            data-transition="fade"
                            data-slotamount="7"
                            class="slider-1 text-center gradient-overlay"
                            data-thumb="img/background/banner-bg.jpg"
                            data-title="EXPERTISE YOU CAN TRUST">
                        <img
                                src="img/background/banner-bg.jpg"
                                data-bgposition="center center"
                                data-kenburns="on"
                                data-duration="20000"
                                data-ease="Linear.easeNone"
                                data-bgfit="100"
                                data-bgfitend="130"
                                data-bgpositionend="center center"
                                alt="slider image">
                        <div
                                class="caption sfb tp-resizeme text-center"
                                data-x="0"
                                data-y="220"
                                data-speed="700"
                                data-start="1000"
                                data-easing="easeOutBack">

                            <div class="date">
                                <?php echo date('Y-M,D - H:A')?>
                            </div>
                        </div>

                        <div
                                class="caption sfb tp-resizeme"
                                data-x="0"
                                data-y="290"
                                data-speed="700"
                                data-start="1500"
                                data-easing="easeOutBack">

                            <h1>Making Dreams A Reality</h1>
                        </div>
                         

                        <ul
                                class="caption sfb tp-resizeme banner-buttons"
                                data-x="0"
                                data-y="400"
                                data-speed="700"
                                data-start="2500"
                                data-easing="easeOutBack">

                            <li class="scrollToLink"><a href="{{ url('create-event') }}" class="colored hvr-bounce-to-right">Create Event Now</a></li>
                            {{--<li class="scrollToLink"><a href="#upcoming-event" class="bordered hvr-bounce-to-right">Get Notification</a></li>--}}

                    </ul>
                    </li>
                    {{--<li--}}
                            {{--data-transition="fade"--}}
                            {{--data-slotamount="7"--}}
                            {{--class="slider-2 text-center gradient-overlay"--}}
                            {{--data-thumb="img/background/subscribe-section-bg.jpg"--}}
                            {{--data-title="EXPERTISE YOU CAN TRUST">--}}

                        {{--<img--}}
                                {{--src="img/background/subscribe-section-bg.jpg"--}}
                                {{--data-bgposition="center center"--}}
                                {{--data-kenburns="on"--}}
                                {{--data-duration="20000"--}}
                                {{--data-ease="Linear.easeNone"--}}
                                {{--data-bgfit="100"--}}
                                {{--data-bgfitend="130"--}}
                                {{--data-bgpositionend="center center"--}}
                                {{--alt="slider image">--}}

                        {{--<div--}}
                                {{--class="caption sfb tp-resizeme text-center"--}}
                                {{--data-x="0"--}}
                                {{--data-y="300"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="1000"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<h1>DONT MISS THE EVENT</h1>--}}
                        {{--</div>--}}

                        {{--<div--}}
                                {{--class="caption sfb tp-resizeme input-box"--}}
                                {{--data-x="0"--}}
                                {{--data-y="370"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="2200"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<ul>--}}
                                {{--<li><span><input type="text" placeholder="Search Events"></span></li>--}}
                                {{--<li><span><input type="text" placeholder="Your Location"></span></li>--}}

                            {{--</ul>--}}
                        {{--</div>--}}

                        {{--<ul--}}
                                {{--class="caption sfb tp-resizeme banner-buttons"--}}
                                {{--data-x="0"--}}
                                {{--data-y="530"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="2500"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<li class="scrollToLink"><a href="#register-now" class="colored hvr-bounce-to-right">Search</a></li>--}}
                            {{--<li class="scrollToLink"><a href="#upcoming-event" class="bordered hvr-bounce-to-right">Get Notification</a></li>--}}

                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li--}}
                            {{--data-transition="fade"--}}
                            {{--data-slotamount="7"--}}
                            {{--class="slider-3 text-center gradient-overlay"--}}
                            {{--data-thumb="img/background/register-bg.jpg"--}}
                            {{--data-title="EXPERTISE YOU CAN TRUST">--}}

                        {{--<img--}}
                                {{--src="img/background/register-bg.jpg"--}}
                                {{--data-bgposition="center center"--}}
                                {{--data-kenburns="on"--}}
                                {{--data-duration="20000"--}}
                                {{--data-ease="Linear.easeNone"--}}
                                {{--data-bgfit="100"--}}
                                {{--data-bgfitend="130"--}}
                                {{--data-bgpositionend="center center"--}}
                                {{--alt="slider image">--}}

                        {{--<div--}}
                                {{--class="caption sfb tp-resizeme text-center"--}}
                                {{--data-x="0"--}}
                                {{--data-y="220"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="1000"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<div class="date">--}}
                                {{--December 31, 2017 23:59pm--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div--}}
                                {{--class="caption sfb tp-resizeme"--}}
                                {{--data-x="0"--}}
                                {{--data-y="290"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="1500"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<h1>WORLD TRADE CONFERENCE</h1>--}}
                        {{--</div>--}}
                        {{--<div--}}
                                {{--class="caption sfb tp-resizeme input-box"--}}
                                {{--data-x="0"--}}
                                {{--data-y="370"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="2200"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<ul>--}}
                                {{--<li><span><input type="text" placeholder="Your Name"></span></li>--}}
                                {{--<li><span><input type="text" placeholder="Your Email"></span></li>--}}
                                {{--<li><span><input type="text" placeholder="Your Phone"></span></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}

                        {{--<ul--}}
                                {{--class="caption sfb tp-resizeme banner-buttons"--}}
                                {{--data-x="0"--}}
                                {{--data-y="450"--}}
                                {{--data-speed="700"--}}
                                {{--data-start="2500"--}}
                                {{--data-easing="easeOutBack">--}}

                            {{--<li class="scrollToLink"><button type="submit" class="colored hvr-bounce-to-right">Register Now</button></li>--}}
                            {{--<li class="scrollToLink"><a href="#upcoming-event" class="bordered hvr-bounce-to-right">Watch Video</a></li>--}}

                        {{--</ul>--}}
                    {{--</li>--}}

                </ul>
            </div>
        </div>
    </section> <!-- /#banner -->

    <!-- #information-bar -->
    <section id="information-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <ul>
                        <li><span class="img-holder"><img src="img/icons/1.png" alt=""></span></li>
                        <li><span><b>Birthday</b> Price Range (₵1000 - ₵2000) </span></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <ul>
                        <li><span class="img-holder"><img src="img/icons/2.png" alt=""></span></li>
                        <li><span><b>Wedding</b> Price Range (₵3000 - ₵5000) </span></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <ul>
                        <li><span class="img-holder"><img src="img/icons/3.png" alt=""></span></li>
                        
                        <li><span><b>Shows</b> Price Range (₵1000 - ₵2000) </span></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <ul>
                        <li><span class="img-holder"><img src="img/icons/2.png" alt=""></span></li>
                        
                        <li><span><b>Private Events</b> Price Range (₵4000 - ₵8000) </span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- /#information-bar -->


    <!-- #upcoming-event -->
    
    <!-- /#upcoming-event -->










    <!-- #blog -->
    
    <!-- /#blog -->


    <!-- #contact -->
    <section id="contact" class="gradient-overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 form-section" style="text-align: center; color: white; background-color: #0b0b0b; opacity:0.7; padding: 10px;">
                    {{--<form class="contact-form" action="http://wp1.themexlab.com/html/event_time/include/sendemail.php">--}}
                        {{--<ul class="clearfix">--}}
                            {{--<li class="half"><input type="text" name="name" placeholder="Your Name"></li>--}}
                            {{--<li class="half"><input type="text" name="email" placeholder="Your Email"></li>--}}
                            {{--<li class="full"><input type="text" name="subject" placeholder="Your Subject"></li>--}}
                            {{--<li class="full"><textarea name="message" placeholder="Your Message here"></textarea></li>--}}
                            {{--<li class="full"><button type="submit" class="hvr-bounce-to-right">Contact Us</button></li>--}}
                        {{--</ul>--}}
                    {{--</form>--}}

                    <h3> Dont be a stranger, we are just a few clicks away</h3>
                    <h4>Talk to Us</h4>

                    {{--<button class="btn hvr-bounce-to-right" href="{{url('contact-us')}}">Contact Us</button>--}}
                </div>

                <button style="text-align: center" class="btn btn-secondary hvr-bounce-to-right" href="{{url('contact-us')}}">Contact Us</button>

            </div>
        </div>
    </section>
    <!-- /#contact -->







    <script>


        $(document).ready(function(){


            var term = $("#search");

                $(term).on('keyup',function() {
                    console.log(term.val());

                $.ajax({
                    url:"api/events-search",
                    data:{

                        term : term.val()

                    },
                    success: function (res) {

                         console.log(res);
//

                    }
                });
            })



        });


    </script>


@endsection


