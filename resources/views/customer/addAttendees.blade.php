@extends('layouts.app')

@section('content')












    <!-- #blog -->
    <section id="blog">
        <div class="container">
            <div class="row">



                <div class=" container col-md-10">
                    <form method="post" action="{{url('post-attendees/'. $event->eid)}}"  class="form-horizontal" id="form" enctype="multipart/form-data" >
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                        
                            <div class="col-md-12">

                                    <div class="horizontal-item content-padding with_border text-center  ">
                                        <div class="item-media"><br>
                                            
                                            <h3>Event Info</h3>
                                        </div>
                                        <div class="item-content">
                                            <h3 class="bottommargin_0 ">
                                                <a href="{{ url('/') }}">{{$event->name}}</a>
                                            </h3>
                                            @var
                                            <p class="small-text highlight">{{ $event->location }}</p><br>
                                            <p class="small-text highlight">{{$event->status}}</p><br>
                
                                            <p class="small-text highlight">{{ $event->created_at}}</p>
                
                
                            <hr>
                                        </div>
                                    </div>
                
                                </div>

                        </div>

                        <div class="row">

                            <h2> Add Attendee </h2>

                            <table id="table" class="col-md-10 table table-hover">
                                <tr>
                                    <th>First Name</th>
                                    <th>last Name</th>
                                    <th>Email</th>
                                    <th class="quantityTH">Phone</th>
                                    <th></th>
                                </tr>

                                <tr>
                                    <td>
                                        <input class="form-control quantity"  type="text" placeholder="first name" required name="fname">
                                    </td>
                                    <td>
                                        <input class="form-control quantity"  type="text" placeholder="surname" name="sname" required>
                                    </td>
                                    <td class="">
                                        <input class="form-control quantity" type="email"  placeholder="email" name="email" required>
                                    </td>
                                    <td class="">
                                        <input class="form-control quantity" type="text" placeholder="phone"  name="phone" required>
                                    </td>
                                    {{--<td><a  class="remove btn btn-danger">Add </a> </td>--}}
                                </tr>
                            </table>

                        </div>

                        <div class="container">
                        </div>


                        <button type="submit" class="btn btn-success">
                            Send Invite
                        </button>

                    </form>

                    <h4> OR Upload a .xls list</h4>


                    <form enctype="multipart/form-data" method="post" action="{{url('upload-attendees/'. $event->eid)}}">
                        {{csrf_field()}}
                        <div>
                            <input type="hidden" name="eid" value="{{$event->eid}}">
                            <input class="btn" type="file" name="file" required>
                        </div>

                        <button class="btn btn-primary">
                            Upload Files
                        </button>

                    </form>


                </div>

                <!-- #banner -->

            </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /#blog -->


    <!-- #contact -->
    {{--<section id="contact" class="gradient-overlay">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<div class="section-title text-center">--}}
                        {{--<h1>Contact us</h1>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12 form-section">--}}
                    {{--<form class="contact-form" action="http://wp1.themexlab.com/html/event_time/include/sendemail.php">--}}
                        {{--<ul class="clearfix">--}}
                            {{--<li class="half"><input type="text" name="name" placeholder="Your Name"></li>--}}
                            {{--<li class="half"><input type="text" name="email" placeholder="Your Email"></li>--}}
                            {{--<li class="full"><input type="text" name="subject" placeholder="Your Subject"></li>--}}
                            {{--<li class="full"><textarea name="message" placeholder="Your Message here"></textarea></li>--}}
                            {{--<li class="full"><button type="submit" class="hvr-bounce-to-right">Contact Us</button></li>--}}
                        {{--</ul>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

@endsection