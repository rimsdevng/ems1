@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Event</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ url('/update-event/'. $event->eid) }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Event Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{$event->name}}" value="{{ old('name') }}"  required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Event Address</label>

                                <div class="col-md-6">
                                    <input id="location" type="text" class="form-control" value="{{$event->location}}" name="location" value="{{ old('location') }}" required>

                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea id="description" type="text" class="form-control" name="description" required>{{$event->description}} </textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Schedule</label>

                                <div class="col-md-6">
                                    <select name="schedule" value="{{$event->schedule}}">
                                        <option value="oneTime"> One Time </option>
                                        <option value="recurring"> Recurring </option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Status</label>

                                <div class="col-md-6">
                                    <select name="status" value="{{$event->status}}">
                                        <option value="draft"> One Time </option>
                                        <option value="onging"> On Going </option>
                                        <option value="ended"> Ended </option>
                                        <option value="protected"> Protected </option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('eventType') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Event Type</label>

                                <div class="col-md-6">
                                    <select name="eventType" value="{{$event->eventType}}">
                                        <option value="public"> Public </option>
                                        <option value="protected"> Protected </option>

                                    </select>
                                    @if ($errors->has('eventType'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('eventType') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('admittance') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">Admittance</label>

                                <div class="col-md-6">
                                    <select name="admittance" value="{{$event->admittance}}">
                                        <option value="free"> Free </option>
                                        <option value="paid"> paid </option>

                                    </select>
                                    @if ($errors->has('eventType'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('eventType') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('publishDate') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Publish Date</label>

                                <div class="col-md-6">
                                    <input id="publishDate" type="date" value="{{$event->publishDate}}" class="form-control" name="publishDate" required>

                                    @if ($errors->has('publishDate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('publishDate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('endDate') ? ' has-error' : '' }}">
                                <label for="endDate" class="col-md-4 control-label">End Date</label>

                                <div class="col-md-6">
                                    <input id="endDate" type="date" value="{{$event->endDate}}" class="form-control" name="endDate" required>

                                    @if ($errors->has('endDate'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('endDate') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Event..
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
