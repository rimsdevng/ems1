@extends('layouts.app')
@include('notifications')
@section('content')
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Register</div>--}}

                    {{--<div class="panel-body">--}}
                        {{--<form class="form-horizontal" method="POST" action="{{ url('/postCreateEvent') }}">--}}
                            {{--{{ csrf_field() }}--}}

                            {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                                {{--<label for="name" class="col-md-4 control-label">Event Name</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                    {{--@if ($errors->has('name'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">--}}
                                {{--<label for="email" class="col-md-4 control-label">Event Address</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="location" type="text" class="form-control" name="location" value="{{ old('location') }}" required>--}}

                                    {{--@if ($errors->has('location'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('location') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">--}}
                                {{--<label for="description" class="col-md-4 control-label">Description</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="description" type="text" class="form-control" name="description" required>--}}

                                    {{--@if ($errors->has('description'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('description') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label for="password-confirm" class="col-md-4 control-label">Schedule</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<select name="schedule">--}}
                                        {{--<option value="oneTime"> One Time </option>--}}
                                        {{--<option value="recurring"> Recurring </option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">--}}
                                {{--<label for="status" class="col-md-4 control-label">Status</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<select name="status">--}}
                                        {{--<option value="draft"> One Time </option>--}}
                                        {{--<option value="onging"> On Going </option>--}}
                                        {{--<option value="ended"> Ended </option>--}}
                                        {{--<option value="protected"> Protected </option>--}}
                                    {{--</select>--}}
                                    {{--@if ($errors->has('status'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('status') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('eventType') ? ' has-error' : '' }}">--}}
                                {{--<label for="status" class="col-md-4 control-label">Event Type</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<select name="eventType">--}}
                                        {{--<option value="public"> Public </option>--}}
                                        {{--<option value="protected"> Protected </option>--}}

                                    {{--</select>--}}
                                    {{--@if ($errors->has('eventType'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('eventType') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('admittance') ? ' has-error' : '' }}">--}}
                                {{--<label for="status" class="col-md-4 control-label">Admittance</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<select name="admittance">--}}
                                        {{--<option value="free"> Free </option>--}}
                                        {{--<option value="paid"> paid </option>--}}

                                    {{--</select>--}}
                                    {{--@if ($errors->has('eventType'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('eventType') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('publishDate') ? ' has-error' : '' }}">--}}
                                {{--<label for="description" class="col-md-4 control-label">Publish Date</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="publishDate" type="date" class="form-control" name="publishDate" required>--}}

                                    {{--@if ($errors->has('publishDate'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('publishDate') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group{{ $errors->has('endDate') ? ' has-error' : '' }}">--}}
                                {{--<label for="endDate" class="col-md-4 control-label">End Date</label>--}}

                                {{--<div class="col-md-6">--}}
                                    {{--<input id="endDate" type="date" class="form-control" name="endDate" required>--}}

                                    {{--@if ($errors->has('endDate'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('endDate') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-6 col-md-offset-4">--}}
                                    {{--<button type="submit" class="btn btn-primary">--}}
                                        {{--Register--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}










    <section class="ls section_padding_top_40 section_padding_bottom_100">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <form class="form-horizontal" method="post" action="{{url('postCreateEvent')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="with_border with_padding">

                                    <h4>
                                        Make Request


                                    </h4>

                                    <hr>
                                    <div class="row form-group">
                                        <label for="eventtitle" class="col-lg-3 control-label">Event Title </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="name" id="reporttitle"placeholder="Give an Appropriate Name">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="eventtype" class="col-lg-3 control-label">Event Category</label>
                                        <div class="col-lg-9">
                                            <select name="ecid" class="form-control"id="eventtype">
                                                @foreach($ecid as $ec)
                                                <option value="{{$ec->ecid}}">{{$ec->name}}</option>
                                                    @endforeach

                                            </select>
                                        </div>

                                    </div>


                                    <div class="row form-group">
                                        <label for="Location" class="col-lg-3 control-label">Event Location</label>
                                        <div class="col-lg-9">
                                            <select name="state" class="form-control"id="location">
                                                <option>Accra</option>
                                                <option>Tema</option>
                                                <option>Kumasi</option>
                                                <option>Cape Coast</option>
                                                <option>Northern Region</option>
                                            </select>
                                        </div>

                                    </div>

                                    {{--<div class="row form-group">--}}
                                        {{--<label for="Location" class="col-lg-3 control-label">Event Schedule</label>--}}
                                        {{--<div class="col-lg-9">--}}
                                            {{--<select name="schedule" class="form-control"id="location">--}}
                                                {{--<option value="oneTime">One Time</option>--}}
                                                {{--<option value="recurring">Recurring</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}

                                    {{--</div>--}}




                                    <div class="row form-group">
                                        <label for="Organizername" class="col-lg-3 control-label">Number of Attendees</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="number_of_attendees" id="reporttitle"placeholder="Number of Attendees">
                                           
                                        </div>
                                    </div>


                                    <div class="row form-group">
                                        <label for="color" class="col-lg-3 control-label">Color </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="color" id="reporttitle"placeholder="Color">

                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="Organizername" class="col-lg-3 control-label">Date </label>
                                        <div class="col-lg-9">
                                            <input type="date" class="form-control" name="publishDate" id="reporttitle"placeholder="date">
                                        </div>
                                    </div>
                                   
                                    <div class="row form-group">
                                        <label for="color" class="col-lg-3 control-label">Time </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" name="time" id="reporttitle"placeholder="time">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label for="color" class="col-lg-3 control-label">Event Type </label>
                                        <div class="col-lg-9">
                                            

                                            <select name="type" class="form-control"id="location">
                                                <option>Formal</option>
                                                <option>Informal</option>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Event Address </label>
                                        <div class="col-lg-9">
                                            <textarea rows="8" name="location" placeholder="Please enter the event address " class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Event Details </label>
                                        <div class="col-lg-9">
                                            <textarea rows="8" name="description" placeholder="Please include relevant details " class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label" for="petition-media">Event Image Banner</label>
                                        <div class="col-lg-9">
                                            <input type="file" name="image" id="petition-media">
                                            <p class="help-block">Select Media</p>

                                        </div>
                                    </div>


                                  


                                    


                                    {{--<div class="row form-group">--}}
                                        {{--<label for="Organizername" class="col-lg-3 control-label">Organizer Name </label>--}}
                                        {{--<div class="col-lg-9">--}}
                                            {{--<input type="text" class="form-control" id="reporttitle"placeholder="Who is organizing this event?">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}


                            {{-- free or payment option --}}

                                    {{-- <div class="radio">
                                        <input type="radio" name="admittance" id="free" value="free" checked="">
                                        <label for="free">
                                            Free Tickets
                                        </label>
                                    </div>

                                    <div class="radio">
                                        <input type="radio" name="admittance" id="paid" value="paid">
                                        <label for="paid">
                                            Paid Tickets
                                        </label>
                                    </div> --}}

                            {{-- end of free or payment option --}}

                                    <!-- .row  -->
                                    {{--  <div id="fee" class="row form-group">
                                        <label for="fee" class="col-lg-3 control-label">Fee</label>
                                        <div class="col-lg-9">
                                            <input type="number" class="form-control" name="cost" id="reporttitle" placeholder="if a paid event">
                                        </div>
                                    </div>  --}}


                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="theme_button wide_button">Create Event</button>
                                            <a href="{{url('profile')}}" class="theme_button inverse wide_button">Cancel</a>
                                        </div>
                                    </div>


                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->


                        </div>
                        <!-- .col-* -->
                    </form>


                </div>
                <!-- .row  -->

                <script>
                    $(document).ready(function () {

                        var paid = $('#paid');
                        var free = $('#free');
                        var fee = $('#fee');

                        fee.addClass('hide');

                        paid.on('change',function () {
                            console.log('paid changed');

                            if(free.checked){
                                fee.addClass('hide');
                            } else {
                                fee.removeClass('hide');
                            }

                        });


                        free.on('change',function () {
                            if(paid.checked){
                                fee.removeClass('hide');
                            } else {
                                fee.addClass('hide');
                            }



                        });


                    });
                </script>

                <div class="col-sm-4">

                </div>
            </div>
        </div>
    </section>

@endsection
