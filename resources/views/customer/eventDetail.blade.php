@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}
                                {{--<a href=" {{url('event-details/'. $event->eid)}}">  <h3>  {{$event->name}}</h3></a></br>--}}
                                {{--<p>{{$event->description}}</p>--}}
                                {{--<p>{{$event->cost}}</p>--}}
                                {{--<span>  {{$event->category->name}}</span>--}}
                                {{--<span>  {{$event->created_at}}</span>--}}
                                {{--<div class="col-md-6">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <!-- #header -->


                        <!-- #page-title -->

                        <section id="page-title" class="gradient-overlay">


                            @if(Auth::check() and  Auth::user()->role == 'admin')
                                <a class="btn btn-danger pull-right" href="{{url('user-events')}}" > Back to List </a>

                            @endif


                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h1 style="color:white">{{$event->name}}</h1>
                                        @if($event->admittance == 'free')
                                        <h3 style="color:white">Free Event</h3>
                                        @else
                                        <h3 style="color:white">&#8373 {{number_format($event->cost,2) }}</h3>
                                    @endif
                                    </div>
                                </div>
                            </div>



                        </section>

                    <!-- #information-bar -->
                        <section id="information-bar">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <ul>
                                            <li><span class="img-holder"><img src="{{url('img/icons/1.png')}}" alt=""></span></li>
                                            <li><span><b>Location</b> {{$event->description}}</span></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <ul>
                                            <li><span class="img-holder"><img src="{{url('img/icons/2.png')}}" alt=""></span></li>
                                            <li><span><b>Organizer</b>{{$event->user->company}}</span></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <ul>
                                            <li><span class="img-holder"><img src="{{url('img/icons/3.png')}}" alt=""></span></li>
                                            <li><span><b>Admittance</b> {{$event->admittance}}</span></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                        <ul>
                                            <li><span class="img-holder"><img src="{{url('img/icons/3.png')}}" alt=""></span></li>
                                            <li><span><b>Starts Date</b> {{$event->publishDate}}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- /#page-title -->

                        {{--Controls for event and  earnings--}}
                            @if(Auth::check() and $event->uid == Auth::user()->uid )


                            <section id="information-bar">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                            <ul>
                                                <li><span class="img-holder"><img src="{{url('img/icons/1.png')}}" alt=""></span></li>
                                                <li><span><b>Attendees</b> {{count($event->attendees)}}</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                            <ul>
                                                <li><span class="img-holder"><img src="{{url('img/icons/2.png')}}" alt=""></span></li>
                                                <li><span><b>Earning</b>&#8373  {{number_format($earnings,2)}}</span></li>
                                            </ul>
                                        </div>

                                        {{--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">--}}
                                            {{--<ul>--}}
                                                {{--<li><span class="img-holder"><img src="{{url('img/icons/3.png')}}" alt=""></span></li>--}}
                                                {{--<li><span><b>Admittance</b> {{$event->admittance}}</span></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">--}}
                                            {{--<ul>--}}
                                                {{--<li><span class="img-holder"><img src="{{url('img/icons/3.png')}}" alt=""></span></li>--}}
                                                {{--<li><span><b>Starts Date</b> {{$event->publishDate}}</span></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                            </section>

                            @endif



                        {{--end of earning and control--}}





                        <!-- #blog -->
                        <section id="blog"  class="blog-archive">
                            <div class="container">

                                <div class="row blog-post-container">

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <!-- .single-post -->
                                            <div class="single-post thumbnail col-lg-12 col-md-12 col-sm-12 wow fadeIn">
                                                <div class="img-holder pull-left">
                                                    @if($event->image)
                                                    <img src="{{$event->image}}" alt=" ">
                                                    @else
                                                        <h2>Image</h2>
                                                    @endif
                                                </div>
                                                <div class="content pull-left">
                                                    <h3>{{$event->name}}</h3>

                                                    <p>{{str_limit($event->description,50)}}</p>

                                                    {{--<a href="#" class="read-more hvr-bounce-to-right">Get Tickets</a>--}}

                                                    {{--class="btn btn-primary"--}}


                                                    @if($event->admittance == 'paid')
                                                    <form>
                                                        <!-- <script src="https://js.paystack.co/v1/inline.js"></script> -->
                                                        {{--<button type="button" onclick="payWithPaystack()"> Pay </button>--}}
                                                        <!-- @if(Auth::guest())
                                                            <a  class="read-more hvr-bounce-to-right" href="{{url('login')}}" type="button" > Get Tickets </a>
                                                        @endif

                                                        @if(Auth::check())
                                                            <button class="read-more hvr-bounce-to-right" type="button" onclick="payWithPaystack()"> Get Tickets </button>
                                                        @endif -->
                                                    </form>

                                                        @endif
                                                    @if($event->admittance =='free')
                                                        @if(Auth::check())
                                                            <form method="post" action="{{url('attend-event/'.$event->eid)}}">
                                                                {{csrf_field()}}

                                                                <button class="btn btn-primary" type="submit"> Get Free Event </button>

                                                            {{--<a  class="read-more hvr-bounce-to-right btn"  type="submit" > Get Tickets </a>--}}
                                                            </form>
                                                        @endif
                                                    @endif


                                                    {{--<form method="post" action="">--}}
                                                        {{--<input type="file" placeholder="Please select a file" >--}}
                                                        {{--<button type="submit"> Upload File</button>--}}
                                                    {{--</form>--}}



                                                </div>
                                                <div class="content pull-left">
                                                    <h3>DATE AND TIME</h3>
                                                    <p class="meta"><a href="#">8:00 AM – 7:00 PM</a> {{$event->publishDate}}
                                                    </p>
                                                    <p>{{$event->location}}</p>

                                                </div>
                                            </div>
                                            <!-- /.single-post -->

                                            <!-- .single-post -->
                                            <div class="single-post thumbnail col-lg-12 col-md-12 col-sm-12 wow fadeIn">

                                                <div class="content pull-left">
                                                    <h3>DESCRIPTION</h3>

                                                    <p>{{$event->description}}</p>

                                                </div>
                                            </div>
                                            <!-- /.single-post -->

                                        </div>
                                    </div>

                                </div>
                                <div class="row blog-post-container">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-lg-push-0 col-md-push-0 col-sm-push-3">


                                    </div>

                                    {{--<!-- .post-pagination -->--}}
                                    {{--<div class="post-pagination text-center">--}}
                                        {{--<ul>--}}
                                            {{--<li><span>1</span></li>--}}
                                            {{--<li><a href="#">2</a></li>--}}
                                            {{--<li><a href="#">3</a></li>--}}
                                            {{--<li><a href="#">4</a></li>--}}
                                            {{--<li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                    <!-- /.post-pagination -->
                                </div>
                            </div>
                        </section>
                        <!-- /#blog -->





            </div>
        </div>
    </div>





    <script>

        function payWithPaystack(){
            var handler = PaystackPop.setup({
                key: 'pk_test_12f3a96b45073a52b31ee54fb71dac29d51eceb3',
                email: 'medubio@gmail.com',
//                email: 'joke@gmail.com',
                amount: '{{($event->cost)*100 }}',
                ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                metadata: {
                    custom_fields: [
                        {
                            eid: "{{$event->eid}}",
                            phone: "{{$event->phone}}"
                        }
                    ]
                },
                callback: function(response){
                    alert('success. transaction ref is ' + response.reference);
                },
                onClose: function(){
                    alert('window closed');

                }
            });
            handler.openIframe();
        }
    </script>
@endsection
