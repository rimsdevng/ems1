<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>Event Sparks</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script>

    {{--<!-- main stylesheet -->--}}
    {{--<link rel="stylesheet" href="{{url('css/style.css')}}">--}}
    {{--<!-- responsive stylesheet -->--}}
    {{--<link rel="stylesheet" href="{{url('css/responsive.css')}}">--}}
    {{--<script type="text/javascript" src="{{url('js/jquery.js')}}"></script>--}}

    {{--home--}}
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    <!-- responsive stylesheet -->
    <link rel="stylesheet" href="{{url('css/responsive.css')}}">

    {{--endhome--}}
    {{--events --}}
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/animations.css')}}">
    <link rel="stylesheet" href="{{url('assets/scss/fonts.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/main.css')}}" class="color-switcher-link">
{{--endevents--}}

    {{--login--}}


    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/animations.css')}}">
    <link rel="stylesheet" href="{{url('assets/scss/fonts.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/main.css')}}" class="color-switcher-link">
    <link rel="stylesheet" href="{{url('assets/css/dashboard.css')}}" class="color-switcher-link">

   
    <script src="{{url('assets/js/vendor/modernizr-2.6.2.min.js')}}"></script>


    <!-- DataTables -->
    <link href="{{ url('backend/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('backend/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ url('backend/datatables/buttons.dataTables.css') }}" rel="stylesheet" type="text/css">



    {{--endlogin--}}



</head>
<body>
    <div id="app">
        <header id="header" style="background-color: #2e3436;">
            <div class="container">
                <div class="row">
                    <!-- .logo -->
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 logo" >
                        <a href="{{url('/')}}"><img style="height: 50px;" src="{{url('img/resources/centralxt_logo.png')}}" alt="Logo Image"></a>
                    </div>
                    <!-- /.logo -->

                    <!-- .mainmenu-container -->
                    <nav class="col-lg-9 col-md-9 col-sm-6 col-xs-6 mainmenu-container">
                        <button class="nav-toggler">Navigation <i class="fa fa-bars"></i></button>
                        <ul class="mainmenu clearfix">
                            <li class="nav-closer"><i class="fa fa-close"></i></li>
                            <li class="current scrollToLink dropdown">
                                <a href="{{url('/')}}">HOME</a>

                            </li>
                            <li>
                                <a href="{{url('events')}}">Browse Events</a>

                            </li>
                           
                            {{--  <li>
                                <a href="{{url('contact-us')}}">Contact Us</a>

                            </li>  --}}

                            @if(Auth::guest())
                            <li class="scrollToLink dropdown">
                                <a href="">Signin/SignUp</a>
                                <ul class="submenu">
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>

                                </ul>
                            </li>
                                @else
                                <li class="scrollToLink dropdown">
                                <li><a href="{{url('create-event')}}">Create  Event</a></li>



                                <li class="scrollToLink dropdown">
                                    <a href="">Hey, {{Auth::user()->fname}}</a>
                                    <ul class="submenu">
                                        @if(Auth::check() and Auth::user()->role == 'admin' or Auth::user()->role == 'admin')

                                        <li><a href="{{ url('admin-dash') }}">Dashboard</a></li>
                                        @endif

                                        @if(Auth::check() and Auth::user()->role == 'user')
                                        <li><a href="{{ url('profile') }}">My Profile</a></li>
                                        {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                                        @endif

                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>


                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>

                                    </ul>
                                </li>
                                </li>
                            @endif
                        </ul>
                    </nav>
                    <!-- /.mainmenu-container -->
                </div>
            </div>
        </header>

        {{--style="margin-top: 100px;"--}}
        <div style="margin-top: 120px;">
        @include('notifications')
            @yield('content')
        </div>

    </div>
    <footer>
        <div class="container">
            <div class="row">
                <!-- .footer-widget -->
                <div class="col-lg-12 col-md-12 col-sm-12 footer-widget about-widget" >
                    <img src="img/resources/centralxt_logo.png" alt="Footer Logo">
                    <small>we make it work..</small>
                    {{--  <p>How It Works<br>

                        Pricing<br>
                        Content Standards<br> For Large Parties & Complex Events <br> Event Pricing
                        Event Standards <br> For Large & Conferences, <br> </p>  --}}
                    <div class="social-icons">
                        <a href="{{ url('/') }}"><i class="fa fa-facebook"></i></a>
                        {{--  <a href="#"><i class="fa fa-twitter"></i></a>  --}}
                        <a href="{{ url('/') }}"><i class="fa fa-google-plus"></i></a>
                        {{--  <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>  --}}
                    </div>
                </div>
                <!-- /.footer-widget -->

                <!-- .footer-widget -->
                {{--  <div class="col-lg-2 col-md-2 col-sm-6 footer-widget menu-widget">  --}}
                    {{--  <h3>QUICK LINKS</h3>  --}}
                    {{--  <ul>
                        <li><a href="#"><i class="fa fa-angle-right"></i> FAQ</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Support</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Event</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Contact Us</a></li>
                    </ul>  --}}
                {{--  </div>  --}}
                <!-- /.footer-widget -->

                <!-- .footer-widget -->
                {{--  <div class="col-lg-3 col-md-3 col-sm-6 footer-widget twitter-feed " >
                    <h3>Twitter Feed</h3>
                    <div class="twitter"></div>
                </div>  --}}
                <!-- /.footer-widget -->

                <!-- .footer-widget -->
                {{--  <div class="col-lg-3 col-md-3 col-sm-6 footer-widget twitter-feed " >
                    <h3>Facebook Feed</h3>
                    <div class="twitter"></div>
                </div>  --}}
                <!-- /.footer-widget -->
            </div>
        </div>
    </footer>
    <!-- /footer -->

    <!-- #bottom-bar -->
    <section id="bottom-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>&copy; <?php echo date('Y') ?> <a href="index-2.html">Event Sparks</a> ALL RIGHTS RESERVED</p>
                </div>
            </div>
        </div>
    </section>


{{--scripts--}}


    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{url('js/countdown.min.js')}}"></script>
    <script src="{{url('js/jquery.easing.min.js')}}"></script>
    <script src="{{url('js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{url('js/jquery.mixitup.min.js')}}"></script>
    <script src="{{url('js/jquery.bxslider.min.js')}}"></script>
    <script src="{{url('js/owl.carousel.min.js')}}"></script>
    <script src="{{url('js/jquery.appear.js')}}"></script>
    <script src="{{url('js/jquery.countTo.js')}}"></script>
    <script src="{{url('js/circle-progress.js')}}"></script>
    <script src="{{url('js/wow.js')}}"></script>
    <script src="{{url('js/validate.js')}}"></script>
    <script src="{{url('js/custom.js')}}"></script>





    <script src="{{url('assets/js/compressed.js')}}"></script>
    <script src="{{url('assets/js/main.js')}}"></script>
    <!-- <script src="{{url('assets/js/switcher.js')}}"></script> -->





    <script src="{{asset('js2/jquery.min.js')}}"></script>
    <script src="{{asset('js2/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('js2/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('js2/countdown.min.js')}}"></script>
    <script src="{{asset('js2/jquery.easing.min.js')}}"></script>
    <script src="{{asset('js2/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('js2/jquery.mixitup.min.js')}}"></script>
    <script src="{{asset('js2/jquery.bxslider.min.js')}}"></script>
    <script src="{{asset('js2/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js2/jquery.appear.js')}}"></script>
    <script src="{{asset('js2/jquery.countTo.js')}}"></script>
    <script src="{{asset('js2/circle-progress.js')}}"></script>
    <script src="{{asset('js2/wow.js')}}"></script>
    <script src="{{asset('js2/validate.js')}}"></script>
    <script src="{{asset('js2/custom.js')}}"></script>

    <script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script>

    
    <!-- Datatables-->
    <script src="{{ url('backend/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('backend/datatables/dataTables.responsive.min.js') }}"></script>
    
    <script src="{{ url('backend/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('backend/datatables/jszip.min.js') }}"></script>
    <script src="{{ url('backend/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('backend/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('backend/datatables/buttons.html5.min.js') }}"></script>
      <script>
        {{-- $(document).ready(function () {
            $('#datatable1').dataTable();
        }); --}}


            $(document).ready(function () {
              $('#datatable1').DataTable({
                  dom: 'Bfrtip',
                  buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                  ],
                });

          });
    </script>


</body>
</html>








