<?php use Illuminate\Support\Facades\Input; use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

<section id="blog">
    <div class="container">
            <div class="row">
                <table id="table" class="col-md-10 table table-hover">
                    <tr>
                        <th>First Name</th>
                        <th>last Name</th>
                        <th>Email</th>
                        <th class="quantityTH">Status</th>
                        <th></th>
                    </tr>
                    @if(count($attendees)> 0)
                    @foreach($attendees as $a)
    
    
                    <tr>
                        <td>
                            {{$a->fname}}
                        </td>
                        <td>
                            {{$a->sname}}
                        </td>
                        <td>
                            {{$a->email}}
                        </td>
                          @if($a->status == 'unpaid')
                        <td style="color: darkred;">  {{$a->status}} </td>
                              @endif
    
                        @if($a->status == 'paid')
                        <td style="color: darkgreen;">  {{$a->status}} </td>
                              @endif
                    </tr>
    
    
    
                    @endforeach
    
                        @endif
    
    
    
                </table>
    
                    @if(count($attendees) == 0)
    
                        <h3 class="text-center"> You have no Attendees yet!</h3>
    
                    @endif
    
                </div>
    </div>
</section>
            
@endsection