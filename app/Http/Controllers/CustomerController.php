<?php

namespace App\Http\Controllers;

//use App\Attendees;
use App\Attendees;
use App\bank;
use App\Event;
use App\EventCat;
use App\EventCategories;
use App\Mail\EventInvitiation;
use App\Mail\FreeEvent;
use App\setting;
use App\User;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use PHPExcel_IOFactory;
use Psy\Exception\Exception;
use Sabberworm\CSS\Settings;
use Symfony\Component\HttpKernel\DependencyInjection\RemoveEmptyControllerArgumentLocatorsPass;

class CustomerController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}




	public function createEvent(  ) {
		$ecid = EventCat::all();
		return view('customer.createEvent',[
			'ecid' => $ecid
		]);
	}

	public function postCreateEvent(Request $request  ) {
		// the old method
		// 	$image = $request->image;

		// 	$new_name = time().$image->getClientOriginalName();

		// 	$image->move('uploads', $new_name);
		//  endof old method

		if($request->hasFile('image')){
			$destinatonPath = '';
			$filename = '';
	
			$file = Input::file('image');
			$destinationPath = public_path().'/uploads/';
			$filename = str_random(6).'_'.$file->getClientOriginalName();
			$uploadSuccess = $file->move($destinationPath, $filename);
		}else {
			$filename = url('img/default-image.png');
		}

		$event = new Event();
		$event->name = $request->input('name');
		$event->ecid = $request->input('ecid');
		$event->uid = Auth::user()->uid;
		$event->description = $request->input('description');
		$event->eventType = $request->input('eventType');
		// $event->image = url('uploads/'.$new_name);
		$event->image = 'uploads'. $filename;
		$event->status = 'ongoing';
		$event->publishDate = $request->input('liveDate');
		$event->endDate  = $request->endDate = $request->input('endDate');
		$event->admittance = $request->input('admittance');
		$event->cost = $request->input('cost');

		// newly added

		$event->number_of_attendees = $request->input('number_of_attendees');
		$event->color = $request->input('color');
		$event->time = $request->input('time');
		$event->type = $request->input('type');



		//		return $event;
		$staus = $event-> save();

		if ($staus){
			$request->session()->flash('success','Event successful created!!');
		}
		return redirect('create-event');


	}


	public function addAttendees(Request $request, $eid ) {
		 $event = Event::find($eid);

		 return view('customer.addAttendees',[
		 	'event' => $event
		 ]);
	}

	public function postAddAttendees( Request $request, $eid ) {

	
		$attendees = new Attendees();
		$attendees->eid = $eid;
		$attendees->fname = $request->input('fname');
		$attendees->sname = $request->input('sname');
		$attendees->email = $request->input('email');
		$attendees->phone = $request->input('phone');
		$attendees->status  = 'unpaid';
		$status = $attendees->save();
		

			$users =  User::all();
			if ($status){
				//Mail::to($request->input('email'))->send( new EventInvitiation, $status);


				$exists = false;
			foreach ($users as $user){
				if( $user->email == $request->input('email')){
					$exists = true;
					break;
				}
			}



			if(!$exists){
				$customer = new  User();
				$customer->fname = $request->input('fname');
				$customer->sname = $request->input('sname');
				$customer->phone = $request->input('phone');
				$customer->email = $request->input('email');
				$customer->password = bcrypt('123456');
				$customer->save();
			}



		


		/* Create a new Excel5 Reader  */



		}
		return redirect('profile');

	}


	public function uploadAttendees(Request $request, $eid) {


		try {


			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/attendees",$inputFileName);

			$downloadUrl = url("/uploads/attendees/".$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/attendees/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/attendees/".$inputFileName);

			$sheet = $objPHPExcel->getActiveSheet();
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);



			$count = 0;
			// add results to data base from file
			foreach($rowData as $cell) {

				$count++;
				try {

					$attendee         = new Attendees();
					$attendee->eid    = $eid;
					$attendee->fname  = $cell[0];
					$attendee->sname  = $cell[1];
					$attendee->email  = $cell[2];
					$attendee->phone  = $cell[3];

					$status =$attendee->save();


				}

				catch(\Exception $e){}

			}

			$request->session()->flash('success',"$count Attendees Uploaded.");


		return redirect('profile');



		}
		catch (\Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


	}

	public function viewAttendees($eid) {
		if (Input::has('by')){
			$by = Input::get('by');

			$attendees = Attendees::where('status' , $by)->where('eid',$eid)->get();
		}else{

			$attendees = Attendees::where( 'eid',$eid )->get();

		}
		$event = Event::find($eid);

		return view( 'customer.viewAttendees', [
			'attendees' => $attendees,
			'event'       =>  $event
		]);
	}


	public function attendeesPDF($eid) {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->attendeesList($eid) );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("Gurudeveloper_Invoice_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}

	}


	public function attendeesList($eid) {
		$attendees = Attendees::find($eid);
		//customer.viewAttendees
		return view('attendeesList',[
			'attendees' => $attendees
		]);
	}

	public function customerProfile(  ) {
		$events = Event::where('uid', Auth::user()->uid)->orderBy('created_at','desc')->paginate(2);

		$event = Event::where('uid', Auth::user()->uid)->get();

		$whole_earning = $this->customerEarnings($event);

		$chargeCap = setting::where('name', 'ChargeCap_Value')->get()->last();
		$percent = setting::where('name', 'ServiceCharge_Percentage')->get()->last();
		
		//taking 10% as the charge from the attendees.

		$charge =  ($whole_earning/100) * $percent->value;

		if($charge > $chargeCap->value ) $charge = $chargeCap->value;

		$payable = $whole_earning - $charge ;
		$earning = $payable;

		// save the  total earnings of the customer.
		$customer = User::find(Auth::user()->uid);
		$customer->TotalEarnings = $earning;
		$customer->save();


		// (value/100) * desired %

		$user = Auth::user()->uid;
		$bank = bank::all();




		return view('customer.profile',[
		'events' => $events,
		'user'   => $user,
		'earnings' => $earning,
		'banks'     => $bank

		]);



	}


	public function customerEarnings($event) {

		$customerEarnings = [];

		foreach ($event as $e){
			$numberOfAttendees = count($e->attendees);
			$amount = $e->cost * $numberOfAttendees;
			array_push($customerEarnings, $amount);
		}
		$p = array_sum($customerEarnings);
		return $p;


	}

	public function editEvent($eid ) {
		$event = Event::find( $eid);
		return view('customer.editEvent',[
			'event' => $event
		]);
	}



	public function updateEvent(Request $request, $eid ) {
		 $event = Event::findorfail($eid);
	//		 return $event;
		$event->update($request->all());
		return redirect('profile');
	}


	public function deleteEvent(Request $request, $eid){

		$status = Event::destroy($eid);

		return redirect('my-profile');

	}


	public function account(  ) {
		$user = User::where('uid', Auth::user()->uid)->get();
		return view('customer.account',[
			'user' => $user
		]);
	}

	public function editAccount(Request $request, $uid) {
		$user = User::where('uid', Auth::user()->uid)->get();
		return view('customer.editAccount',[
			'user' => $user
		]);

	}

	public function updateAccount(Request $request, $uid  ) {
		$user = User::where('uid', Auth::user()->uid)->get();

		$user->Update($request->all());

	}

	public function attendEvent(Request $request, $eid) {
	//		$eid = $request->eid;
		$user = Auth::user();
		$email = $user->email;

		$event = Event::find($eid);

		$attend =  new Attendees();
		$attend->eid = $eid;
		$attend->fname = $user->fname;
		$attend->sname = $user->sname;
		$attend->email = $user->email;
		$attend->phone = $user->phone;
		$attend->status = 'free';

		//		return $attend;

		$status = $attend->save();

		if ($status){

			Mail::to($email)->send( new FreeEvent($event,$user) );
		$request->session()->flash('success','thanks for showing interest an electronic ticket has been mailed to you. ');
		}

		return redirect()->back();

	}


	public function updateUser(Request $request, $uid  ) {
		$user = User::find($uid);

		$user->update($request->all());

		return redirect('profile');

	}


}
