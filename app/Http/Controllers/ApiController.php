<?php

namespace App\Http\Controllers;

use App\Attendees;
use App\Event;
use App\Mail\paymentConfirmation;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller
{
    //

	public function confirmPayment(Request $request  ) {


		try {
			DB::transaction( function () {

				global $request;

				Mail::to("jonahrchirika@.com")->send( new paymentConfirmation( json_encode($request->all()) ) );

				$response = json_decode( json_encode( $request->all() ) );


				// check if payment was successful

				if ( $response->event == "charge.success" ) {

					$data = $response->data;

					$email     = $data->customer->email;
					$amount    = $data->amount / 100;
					$reference = $data->reference;
					$eid        = $data->eid;

					$user = User::where( 'email', $email )->first();

					$previousPayment = Payment::where( 'uid', $user->uid )->get()->last();

					if ( isset( $previousPayment ) ) {
						if ( $previousPayment->others != $reference ) { // check if user has been credited for transaction already

							$payment         = new payment();
							$payment->amount = $amount;
							$payment->method = 'card';
							$payment->uid    = $user->uid;
							$payment->others = $reference;
							$payment->save();
							Mail::to( $email )->send( new paymentConfirmation( $data, $user ) );


						} // end previous payment check

					} else {

						$guestlist = new Attendees();
						$guestlist->name = $user->name;
						$guestlist->email = $user->email;
						$guestlist->phone = $user->phone;
						$guestlist->eid = $eid;
						$guestlist->paid = 1;
						// add to guest list if not exists
						// else change entry to paid

						$payment         = new payment();
						$payment->amount = $amount;
						$payment->method = 'card';
						$payment->uid    = $user->uid;
						$payment->others = $reference;
						$payment->save();

						Mail::to( $email )->send( new cardPaymentSuccessful( $data, $user ) );

					}
				}

				return http_response_code( 200 );

			}, 3 );

		} catch ( \Exception $exception ) {
			return http_response_code(500);
		}



	}

	public function events() {

		if (Input::has('term')){
			$term = Input::get('term');

			$events = Event::where('name', 'like', "%$term%")->get();

			return $events;

		} else {
//			return [];
		$events = Event::all();
		}


	}


	
}
