<?php

namespace App\Http\Controllers;

use App\Attendees;
use App\Event;
use App\setting;
use App\User;
use App\EventCat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

//use App\Http\Middleware\AdminMiddleware;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
//        $this->middleware('AdminMiddleware');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }




	public function settings(  ) {


		$charge_percent = setting::where('name', 'ServiceCharge_Percentage')->get()->last();
		$cap_charge     = setting::where('name','ChargeCap_Value')->get()->last();
    	return view('admin.settings',[
    		'charge' => $charge_percent,
		    'cap'    => $cap_charge
	    ]);
    }


	public function postSettings( Request $request ) {

    	$settings = new setting();
    	$settings->name  = $request->input('name');
    	$settings->value = $request->input('value');
    	$settings->save();

    	return redirect('settings');

	}


	public function settingsHistory(  ) {

    	$settings = setting::orderBy('created_at','DESC')->get();

    	return view('admin.settings_history',[
    		'settings' => $settings

    		]);
	}




	public function eventCategory(  ) {

		return view('customer.eventCategory');
	}

	public function postEventCategory( Request $request  ) {
		$cat = new  EventCat();
		$cat->name = $request->input('name');
		$cat->Description = $request->input('description');
		$cat->uid = Auth::user('admin')->uid;
		$status = $cat->save();
    }
    

    public function dashBoard(){
        $events = Event::orderBy('created_at','desc')->paginate(5);
        $users = User::where('role', 'user')->orderBy('created_at','desc')->paginate(5);
        return view('admin.dash',[
            'users' => $users,
            'events' => $events
        ]);



    }

	public function userEvents(  ) {
//
    	if (Input::has('term')){
    		$term = Input::get('term');
    		$events = Event::where('name', 'like',"%$term%" )->where('status', 'ongoing')->paginate(5);
	    }else{

    		$events = Event::orderBy('created_at','desc')->where('status','ongoing')->paginate(10);
	    }
		return view('admin.events',[
			'events' => $events
		]);
    }

	public function getCustomers(  ) {

    	if (Input::has('term')){
    		$term = Input::get('term');

    		$customers = User::where('role','user')
		                     ->where('fname',  'like', "%$term%")->paginate(5);
	    }else{

    	$customers = User::where('role','user')->paginate(10);
	    }
    	return view('admin.customers',[
    		'customers' => $customers
	    ]);

//		return $customers;
}


	public function banEvent(Request $request,$eid) {

//    	$eid = $request->input('eid');

		$event = Event::find($eid);
		$event->status = 'banned';
		$status = $event->save();


		if ($status){
			$request->session()->flash( 'success', 'Event Successfully Banned');
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

//		Mail::to('sunny.asar@gurudeveloperinc.com')->send(new banEmail());

            return redirect()->back();
    }


	public function showBanned() {
		$events = Event::where('status','banned')->get();
		return view('admin.bannedEvents',[
			'events' => $events
		]);


//		return $events;
    }




//	public function showBanned(  ) {
//
//    	return view('home');
//}



	public function unbanEvent(Request $request,$eid) {

    	$event = Event::find($eid);

    	$event->status = 'ongoing';
		$status = $event->save();


		if ($status){
			$request->session()->flash( 'success', 'Event Successfully restored' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

//		Mail::to('sunny.asar@gurudeveloperinc.com')->send(new banEmail());

		return redirect()->back();
	}

	public function approveEvent(Request $request,$eid) {

    	$event = Event::find($eid);

    	$event->status = 'ongoing';
		$status = $event->save();


		if ($status){
			$request->session()->flash( 'success', 'Event Successfully Approved' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

//		Mail::to('sunny.asar@gurudeveloperinc.com')->send(new banEmail());

		return redirect()->back();
	}

	public function makeStaff(Request $request,$uid) {
		$user = User::find($uid);
		$user->role = 'staff';
		$status =$user->save();

		if ($status){
			$request->session()->flash('success', 'Role successfully changed to Staff');
		}else{
			$request->session()->flash('error', 'Sorry something went wrong.');
		}
			return redirect()->back();
	}

	public function makeAdmin( Request $request,$uid ) {

		$user = User::find($uid);
		$user->role = 'admin';
		$status =$user->save();

		if ($status){
			$request->session()->flash('success', 'Role successfully changed to Administrator');
		}else{
			$request->session()->flash('error', 'Sorry something went wrong.');
		}
		return redirect()->back();


	}







}
