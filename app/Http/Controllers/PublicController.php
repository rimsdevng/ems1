<?php

namespace App\Http\Controllers;

use App\Attendees;
use App\contact;
use App\Event;
use App\EventCat;
use App\setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PublicController extends Controller
{
	public function __construct(Request $request) {
//
//		Session::set('key',$request->fullUrl());
//		echo $request->fullUrl();
	}

	public function getContact(  ) {
		return view('customer.contact');
}


	public function postContact(Request $request  ) {

//		$mail = new contact();
//		$mail->name = $request->input('name');
//		$mail->email = $request->email;
//		$mail->message = $request->message;

		$name = $request->input('name');
		$email = $request->email;
		$message = $request->message;

		$done = Mail::to('jonahrchirika@gmail.com')->send(new contact($name,$email,$message));

		if ($done)
		$request->session()->flash('success' ,' Thank you for getting in touch with us');

		else
			$request->session()->flash('error', 'sorry something went wrong');
}

	public function index() {

		if (Input::has('term')){
    		$term = Input::get('term');
    		$events = Event::where('name', 'like',"%$term%" )->where('status', 'ongoing')->paginate(5);
	    }else{

    		// $events = Event::orderBy('created_at','desc')->where('status','ongoing')->paginate(10);
			$events = Event::where('status', 'ongoing')->orderBy('created_at','desc')->paginate(8);
	    }

		return view('customer.home',[
			'events' => $events
		]);
//		}

	}


	public function viewEvent(  ) {
//
//		if (input::has('term')){
//			$term = input::get('term');
//
//			$events = Event::where('name', 'like', "%$term%")
//			               ->where('status','ongoing')->paginate(3);
//		}else{


		if (Input::has('cat')){
			$cat = Input::get('cat');
				$events = Event::where('ecid',$cat)->orderByDesc('created_at')->get();

//				return $events;
		}elseif (Input::has('price')){
			$price = Input::get('price');

			$events = Event::where('admittance', $price)->orderByDesc('created_at')->get();
		} else{
			$events = Event::where('status', 'ongoing')->orderByDesc('created_at')->get();

		}

		$cats = EventCat::all();
		return view('customer.eventList',[
			'events' => $events,
			'categories' =>$cats

		]);
//		}

	}

	//the other details.

	public function viewEventDetails( Request $request, $eid  ) {



		$event = Event::find($eid);
		$percent = setting::where('name','ServiceCharge_Percentage')->get()->last();
//
//		$percent = $percent->value;
//

//		return $percent;



		$attendees = count(Attendees::where('eid', $eid)->get());

		$costPerHead = $event->cost;

		$totalEventEarning = $attendees * $costPerHead;

		$payable = $totalEventEarning - ($totalEventEarning/100 * $percent->value);



		$earnings = $payable;

//
//
//		$earning = $event;
//		$earning->eventEarnings = $earnings;
//		$earnings->totalEventEarning = $totalEventEarning;
//		$earning->save();


//		return $earnings;
//
		return view('customer.eventDetail',[
			'event' => $event,
			'earnings'  => $earnings

		]);

//		return $payable;








	}

}
