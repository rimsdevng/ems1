<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCat extends Model
{
    protected  $primaryKey = 'ecid';

    protected  $table = 'event_categories';


	public function Event(  ) {
		return $this->hasMany(Event::class,'ecid');
    }
}
