<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $guarded = [ ];
	protected $primaryKey = 'uid';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


	public function Event(  ) {
		return $this->hasMany(Event::class,'uid');
    }

	public function Bank(  ) {

		return $this->belongsTo(bank::class,'bid', 'bid');
    }
}
