<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

	protected $primaryKey ='eid';
	 protected  $table = 'events';
	protected $guarded = [];

	public function Category(  ) {
		return $this->belongsTo(EventCat::class,'ecid');
	 }

	public function Attendees(  ) {

		return $this->hasMany(Attendees::class, 'eid');
	 }

	public function User( ) {
		return $this->belongsTo(User::class, 'uid');
	 }

}
