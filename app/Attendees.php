<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendees extends Model
{
	protected  $primaryKey = 'aid';
	public  $table = 'attendees';


	public function Event(  ) {
		return $this->belongsTo(Event::class, 'eid');
	}

}
