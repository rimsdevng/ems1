<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bank extends Model
{
    protected $primaryKey = 'bid';
    protected $table  = 'bank';


	public function User( ) {

		return $this->hasMany(User::class, 'bid', 'bid');

    }
}
