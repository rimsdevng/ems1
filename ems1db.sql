-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 25, 2019 at 08:57 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems1db`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendees`
--

DROP TABLE IF EXISTS `attendees`;
CREATE TABLE IF NOT EXISTS `attendees` (
  `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('paid','unpaid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unpaid',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendees`
--

INSERT INTO `attendees` (`aid`, `eid`, `fname`, `sname`, `email`, `phone`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'danladi', 'kifasi', 'dankifasji@gmail.com', '08065544992', 'unpaid', '2019-04-09 17:39:30', '2019-04-09 17:39:30'),
(2, 1, 'danladi', 'kifasi', 'dajnkifasi@gmail.com', '8065544992', 'unpaid', '2019-04-09 17:39:57', '2019-04-09 17:39:57'),
(3, 4, 'Mathew', 'Edubio', 'amostimothy7@gmail.com', '8171624968', 'unpaid', '2019-05-15 10:54:53', '2019-05-15 10:54:53');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
CREATE TABLE IF NOT EXISTS `bank` (
  `bid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bid`, `name`, `created_at`, `updated_at`) VALUES
(1, 'GT BANK', '2019-04-01 23:00:00', '2019-04-01 23:00:00'),
(2, 'UBA', '2019-04-01 23:00:00', '2019-04-01 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `eid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ecid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` decimal(8,2) DEFAULT NULL,
  `lat` decimal(8,2) DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` enum('oneTime','recurring') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('draft','ongoing','ended','protected','approved','banned') COLLATE utf8mb4_unicode_ci DEFAULT 'draft',
  `eventType` enum('public','protected') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publishDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endDate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admittance` enum('free','paid') COLLATE utf8mb4_unicode_ci DEFAULT 'free',
  `cost` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_of_attendees` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('formal','informal') COLLATE utf8mb4_unicode_ci DEFAULT 'formal',
  PRIMARY KEY (`eid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`eid`, `ecid`, `uid`, `name`, `location`, `image`, `state`, `lon`, `lat`, `description`, `schedule`, `status`, `eventType`, `publishDate`, `endDate`, `admittance`, `cost`, `created_at`, `updated_at`, `number_of_attendees`, `color`, `time`, `type`) VALUES
(4, 1, 1, 'Birthday for my son', NULL, 'uploadshttp://localhost:8000/img/default-image.png', NULL, NULL, NULL, 'it is a birthday', NULL, 'approved', NULL, NULL, NULL, 'free', NULL, '2019-05-15 10:52:14', '2019-05-17 13:01:20', NULL, NULL, NULL, 'formal'),
(3, 1, 1, 'Chiuke Turned Five', NULL, 'uploadshttp://localhost:8000/img/default-image.png', NULL, NULL, NULL, 'Simple birthday for my child', NULL, 'ongoing', NULL, NULL, NULL, 'free', 0, '2019-05-15 10:10:04', '2019-05-15 10:10:04', NULL, NULL, NULL, 'formal'),
(5, 3, 1, 'Conference Meeting', NULL, 'uploadshttp://localhost:8000/img/default-image.png', NULL, NULL, NULL, 'A business men conference.', NULL, 'ongoing', NULL, NULL, NULL, NULL, NULL, '2019-05-24 09:10:24', '2019-05-24 09:10:24', '30', 'All Gold', '3pm', 'formal');

-- --------------------------------------------------------

--
-- Table structure for table `event_categories`
--

DROP TABLE IF EXISTS `event_categories`;
CREATE TABLE IF NOT EXISTS `event_categories` (
  `ecid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ecid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_categories`
--

INSERT INTO `event_categories` (`ecid`, `name`, `description`, `uid`, `created_at`, `updated_at`) VALUES
(1, 'Birthday', 'Celebration of human\'s birth', 1, '2019-04-02 23:00:00', '2019-04-02 23:00:00'),
(2, 'Wedding', 'Solemnization of two souls', 1, '2019-04-02 23:00:00', '2019-04-02 23:00:00'),
(3, 'Others', 'Other forms of event', 1, '2019-05-23 23:00:00', '2019-05-23 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_11_18_172558_intial', 1),
(3, '2018_10_07_231122_settings', 2),
(4, '2019_04_02_095904_bank', 3),
(5, '2019_05_15_091802_add_to_event', 4),
(6, '2019_05_24_094005_add_to_events_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `payid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `others` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`payid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `sid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` enum('ChargeCap_Value','ServiceCharge_Percentage') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ChargeCap_Value',
  `value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`sid`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'ChargeCap_Value', 0, '2019-04-01 23:00:00', '2019-04-01 23:00:00'),
(2, 'ServiceCharge_Percentage', 1, '2019-04-01 23:00:00', '2019-04-01 23:00:00'),
(3, 'ServiceCharge_Percentage', 2, '2019-04-02 14:34:52', '2019-04-02 14:34:52'),
(4, 'ChargeCap_Value', 5, '2019-04-02 14:35:20', '2019-04-02 14:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `sid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TotalEarnings` decimal(10,0) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `bid`, `fname`, `sname`, `phone`, `company`, `account_no`, `TotalEarnings`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Matthew', 'EB', '0238855067', NULL, '0176600406', '0', 'jonahrchirika@gmail.com', 'user', '$2y$10$mEoVZmFcmChB9HpHhe5ICuKFRMY67o9C87xgClzOj7StQslQKJpvW', 'e3Toi6oaRjF21v7PeW5jZES92zbYaV6FDEOyUwxRq1Fe4MfkzjGlzc08LsxG', '2019-04-02 00:08:19', '2019-04-02 19:10:09'),
(2, NULL, 'Miriam', 'Daniel', '0238855045', NULL, NULL, '0', 'miriamdaniel@gmail.com', 'admin', '$2y$10$w/Fz7ZUxIrRqfTMIjXLb3udj.gauHs20eDDjAAtci3uBCD3LvXPc.', 'sjTnn3iFT3A265isqT9RlksXVokuEcJ6gYrAySFUbjFjgineNZSY1GGnM2Yv', '2019-05-15 07:27:20', '2019-05-15 07:29:30'),
(3, NULL, 'Musa', 'Jackson', '0238855034', NULL, NULL, NULL, 'musajackson@gmail.com', 'user', '$2y$10$ryEBeNEomIy1HC3v/fcBtOT3XGgCf13RZ/MKeJHZFNZTIzlzjA4qS', 'R3O6C9EQtvr6M9ZLGqWRss6UPTFSl2sI4CjEIxNvlKa3W14UyxWuQ7wCPuXh', '2019-05-15 09:59:29', '2019-05-15 09:59:29'),
(4, NULL, 'Mathew', 'Edubio', '8171624968', NULL, NULL, NULL, 'amostimothy7@gmail.com', 'user', '$2y$10$Bp0/P0lfpmySSeBd6JAHUeLuQoIWlLKkY9buSXgWkFs4DIfQolozO', NULL, '2019-05-15 10:54:53', '2019-05-15 10:54:53');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
